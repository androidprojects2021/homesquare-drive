ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
/Users/azinova/Desktop/Live Projects/Dailyhelp_driver_
                           -                          

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From Copy of Emaid:
* .DS_Store
* .idea/
* .idea/Copy of Emaid.iml
* .idea/compiler.xml
* .idea/encodings.xml
* .idea/misc.xml
* .idea/modules.xml
* .idea/workspace.xml
* ic_launcher-web.png
* proguard-project.txt
From SlideMenu:
* SlideMenu.iml
* pom.xml
From android-support-v7-appcompat:
* .DS_Store
* README.txt
* android-support-v7-appcompat.iml
From google-play-services_lib:
* README.txt
* google-play-services_lib.iml
* proguard.txt

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In android-support-v7-appcompat:
* AndroidManifest.xml => androidsupportv7appcompat/src/main/AndroidManifest.xml
* assets/ => androidsupportv7appcompat/src/main/assets
* libs/android-support-v4.jar => androidsupportv7appcompat/libs/android-support-v4.jar
* lint.xml => androidsupportv7appcompat/lint.xml
* res/ => androidsupportv7appcompat/src/main/res/
* src/ => androidsupportv7appcompat/src/main/java
* src/.readme => androidsupportv7appcompat/src/main/resources/.readme
In SlideMenu:
* AndroidManifest.xml => slideMenu/src/main/AndroidManifest.xml
* assets/ => slideMenu/src/main/assets
* lint.xml => slideMenu/lint.xml
* res/ => slideMenu/src/main/res/
* src/ => slideMenu/src/main/java/
In google-play-services_lib:
* AndroidManifest.xml => googleplayservices_lib/src/main/AndroidManifest.xml
* assets/ => googleplayservices_lib/src/main/assets
* libs/google-play-services.jar => googleplayservices_lib/libs/google-play-services.jar
* lint.xml => googleplayservices_lib/lint.xml
* res/ => googleplayservices_lib/src/main/res/
* src/ => googleplayservices_lib/src/main/java/
In Copy of Emaid:
* AndroidManifest.xml => copyofEmaid/src/main/AndroidManifest.xml
* assets/ => copyofEmaid/src/main/assets
* libs/acra-4.5.0.jar => copyofEmaid/libs/acra-4.5.0.jar
* libs/android-async-http-1.4.3.jar => copyofEmaid/libs/android-async-http-1.4.3.jar
* libs/emaidazinova.jar => copyofEmaid/libs/emaidazinova.jar
* libs/gcm.jar => copyofEmaid/libs/gcm.jar
* libs/gson-2.2.4.jar => copyofEmaid/libs/gson-2.2.4.jar
* libs/universal-image-loader-1.8.6-with-sources.jar => copyofEmaid/libs/universal-image-loader-1.8.6-with-sources.jar
* lint.xml => copyofEmaid/lint.xml
* res/ => copyofEmaid/src/main/res/
* src/ => copyofEmaid/src/main/java/
* src/.DS_Store => copyofEmaid/src/main/resources/.DS_Store
* src/com/.DS_Store => copyofEmaid/src/main/resources/com/.DS_Store
* src/com/emaid/.DS_Store => copyofEmaid/src/main/resources/com/emaid/.DS_Store
* src/com/emaid/services/.DS_Store => copyofEmaid/src/main/resources/com/emaid/services/.DS_Store
* src/com/emaid/utils/.DS_Store => copyofEmaid/src/main/resources/com/emaid/utils/.DS_Store

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
