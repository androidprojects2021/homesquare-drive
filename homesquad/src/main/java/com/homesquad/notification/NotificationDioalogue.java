package com.homesquad.notification;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.homesquad.database.customer.SynchronizeCustomer;
import com.homesquad.driver.R;

public class NotificationDioalogue {

    static Dialog dialog1;

    public static void Notification(final Context _context, String _strHeading, String _strMessage) {


        dialog1 = new Dialog(_context);

        dialog1.setContentView(R.layout.customdialogue_error);

        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog1.setCancelable(true);
        TextView textViewHeading = (TextView) dialog1.findViewById(R.id.customeerror_heading);
        TextView textViewContent = (TextView) dialog1.findViewById(R.id.customeerror_message);
        Button buttonOk = (Button) dialog1.findViewById(R.id.customeerror_btn);
        textViewHeading.setText(_strHeading.trim());
        textViewContent.setText(_strMessage.trim());

        buttonOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                NotificationManager nMgr = (NotificationManager) _context.getSystemService(_context.NOTIFICATION_SERVICE);
                nMgr.cancelAll();
                SynchronizeCustomer.diologueSynchronize(_context, "1");
                dialog1.dismiss();

            }
        });

        dialog1.show();

    }

    public static void NotificationNew(final Context _context, String _strHeading, String _strMessage, String notificationData) {


        dialog1 = new Dialog(_context);

        dialog1.setContentView(R.layout.notification_dialog);

        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

//        NotificationPojo details = new Gson().fromJson(_strMessage, NotificationPojo.class);

        String[] string = _strMessage.split(":");

        Log.e(" Notiifcation ", _strMessage);



        dialog1.setCancelable(true);
        TextView textViewHeading = dialog1.findViewById(R.id.customeerror_heading);
        TextView textViewContent = dialog1.findViewById(R.id.customeerror_message);
        TextView shiftTextView = dialog1.findViewById(R.id.shift);
        TextView areaTextView = dialog1.findViewById(R.id.area);
        Button buttonOk = dialog1.findViewById(R.id.customeerror_btn);
        ImageView imageView = dialog1.findViewById(R.id.typeImage);


        textViewHeading.setText(_strHeading.trim().toUpperCase());
//        textViewContent.setText(String.format("Maid     : %s", string[1].replace(", Customer", "")));
        textViewContent.setText(String.format("Customer : %s", string[2].replace(", Shift", "")));
        shiftTextView.setText(String.format("Maid     : %s", string[1].replace(", Customer", "")));

//        areaTextView.setText(String.format("Time : %s", string[3] + string[4] + " - " + string[5]));
        String[] timeString = _strMessage.split("Shift :");
        areaTextView.setText(String.format("Time : %s", timeString[1]));


        if (_strHeading.trim().equalsIgnoreCase("update_booking")) {
            imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.rotate));
        }


        if (_strMessage.contains("New")) {
           imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.new_booking));
        } else if (_strMessage.contains("Updated")) {
           imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.booking_change));
        } else if (_strMessage.contains("Cancelled")) {
           imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.booking_cancel));
        } else if (_strMessage.contains("Delete")) {
           imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.booking_cancel));
        } else if (_strMessage.contains("Transfer")) {
           imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.transfer_maid));
        }

        buttonOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                NotificationManager nMgr = (NotificationManager) _context.getSystemService(_context.NOTIFICATION_SERVICE);
                if (nMgr != null) {
                    nMgr.cancelAll();
                }
                SynchronizeCustomer.diologueSynchronize(_context, "1");
                dialog1.dismiss();

            }
        });

        dialog1.show();

    }


}
