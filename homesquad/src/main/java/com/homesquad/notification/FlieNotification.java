package com.homesquad.notification;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;

public class FlieNotification {
	private static final String FILENAME = "notification.txt";

	public static void writeToFile(Context oContext, String data)
	{
		try
		{
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(oContext.openFileOutput(FILENAME, Context.MODE_PRIVATE));
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		}
		catch (IOException e)
		{
			Log.e("354345", "File write failed: " + e.toString());
		}

	}

	public static String readFromFile(Context oContext)
	{

		String ret = "";

		try
		{
			InputStream inputStream = oContext.openFileInput(FILENAME);

			if (inputStream != null)
			{
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null)
				{
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		}
		catch (FileNotFoundException e)
		{
			Log.e("353245", "File not found: " + e.toString());
		}
		catch (IOException e)
		{
			Log.e("4353455", "Can not read file: " + e.toString());
		}

		return ret;
	}

}
