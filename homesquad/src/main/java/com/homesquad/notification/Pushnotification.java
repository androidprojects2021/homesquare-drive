//package com.emaid.notification;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.AsyncTask;
//import android.util.Log;
//import android.widget.Toast;
//
//import AlertDialogManager;
//import CommonUtilities;
//import ConnectionDetector;
//import ServerUtilities;
//import WakeLocker;
//import com.google.android.gcm.GCMRegistrar;
//
//public class Pushnotification {
//
//	private static Activity _activity;
//
//	private static ConnectionDetector cd;
//	private static AlertDialogManager alert = new AlertDialogManager();
//
//	private static AsyncTask<Void, Void, Void> mRegisterTask;
//
//	public static void initPush(Activity a)
//	{
//		_activity=a;
//		cd = new ConnectionDetector(_activity);
//
//		// Check if Internet present
//		if (!cd.isConnectingToInternet())
//		{
//			// Internet Connection is not present
//			alert.showAlertDialog(_activity, "Internet Connection Error", "Please connect to working Internet connection", false);
//			// stop executing code by return
//			return;
//		}
//
//		// Check if GCM configuration is set
//		if (CommonUtilities.SERVER_URL == null || CommonUtilities.SENDER_ID == null || CommonUtilities.SERVER_URL.length() == 0 || CommonUtilities.SENDER_ID.length() == 0)
//		{
//			// GCM sernder id / server url is missing
//			alert.showAlertDialog(_activity, "Configuration Error!", "Please set your Server URL and GCM Sender ID", false);
//			// stop executing code by return
//			return;
//		}
//
//		// Make sure the device has the proper dependencies.
//		GCMRegistrar.checkDevice(_activity);
//
//		// Make sure the manifest was properly set - comment out this line
//		// while developing the app, then uncomment it when it's ready.
//		GCMRegistrar.checkManifest(_activity);
//
//		_activity.registerReceiver(mHandleMessageReceiver, new IntentFilter(CommonUtilities.DISPLAY_MESSAGE_ACTION));
//
//		// Get GCM registration id
//		final String regId = GCMRegistrar.getRegistrationId(_activity);
//
//		System.out.println("***regId******" + regId);
//		// Check if regid already presents
//		if (regId.equals(""))
//		{
//			// Registration is not present, register now with GCM
//
//			GCMRegistrar.register(_activity, CommonUtilities.SENDER_ID);
//		}
//		else
//		{
//			// Device is already registered on GCM
//			if (GCMRegistrar.isRegisteredOnServer(_activity))
//			{
//				// Skips registration.
//
//
//				Toast.makeText(_activity, "Already registered with GCM", Toast.LENGTH_LONG).show();
//			}
//			else
//			{
//				// Try to register again, but not in the UI thread.
//				// It's also necessary to cancel the thread onDestroy(),
//				// hence the use of AsyncTask instead of a raw thread.
//				final Context context = _activity;
//				mRegisterTask = new AsyncTask<Void, Void, Void>()
//				{
//
//					@Override
//					protected Void doInBackground(Void... params)
//					{
//						// Register on our server
//						// On server creates a new user
//						ServerUtilities.register(context, regId);
//						return null;
//					}
//
//					@Override
//					protected void onPostExecute(Void result)
//					{
//						mRegisterTask = null;
//
//					}
//
//				};
//				mRegisterTask.execute(null, null, null);
//			}
//		}
//
//	}
//
//	private static final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver()
//	{
//		@Override
//		public void onReceive(Context context, Intent intent)
//		{
//			String newMessage = intent.getExtras().getString(CommonUtilities.EXTRA_MESSAGE);
//			// Waking up mobile if it is sleeping
//			WakeLocker.acquire(_activity);
//
//			/**
//			 * Take appropriate action on this message depending upon your app
//			 * requirement For now i am just displaying it on the screen
//			 * */
//			if (newMessage.equals("success"))
//			{
//
//			}
//			else
//			{
//
//			}
//			WakeLocker.release();
//
//		}
//	};
//
//	public static void  ondestory(Context oContext)
//	{
//		if (mRegisterTask != null) {
//			mRegisterTask.cancel(true);
//		}
//		try {
//			if(mHandleMessageReceiver != null){
//				oContext.unregisterReceiver(mHandleMessageReceiver);
//			}
//			GCMRegistrar.onDestroy(oContext);
//		} catch (Exception e) {
//			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
//		}
//	}
//
//}
