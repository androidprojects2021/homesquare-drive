package com.homesquad;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.aretha.slidemenu.SlideMenu;
import com.aretha.slidemenu.SlideMenu.LayoutParams;
import com.google.firebase.iid.FirebaseInstanceId;
import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.database.maid.MySQLiteHelper;
import com.homesquad.driver.R;
import com.homesquad.location.LocationService;
import com.homesquad.notification.Myservices;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;

public class HomepageActivity extends BaseActivity implements BaseInterface {

    public SlideMenu slideMenu;
    public View secondaryMenu, primaryMenu;
    LayoutInflater inflater;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startService(new Intent(this, LocationService.class));
        URLUtil.activateSharepreference(HomepageActivity.this);
//		LocationAlarm.initAlarm(HomepageActivity.this);

        MySQLiteHelper mySQLiteHelper = new MySQLiteHelper(
                HomepageActivity.this);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                setContentView(R.layout.activity_homepage);

            }
        });


        initUI();
        clickFunction();

        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().getString("message") != null) {

                Intent myIntent = new Intent(this, Myservices.class);
                myIntent.putExtra("message", getIntent().getExtras().getString("message"));
                myIntent.putExtra("heading", "Notification");
                startService(myIntent);
            }

        }

//		if (Locationservices.servicesConnected(HomepageActivity.this)) {
//
//		} else {
//			CommonUtil.CustomeDialogue(HomepageActivity.this, "Location Issue",
//					"Google Play Services is not enable");
//		}

        locationDiable();
        try {
            if (getIntent().getExtras().getString("page")
                    .equals("notification")) {
                CommonUtil.synchrinoouserror(HomepageActivity.this,
                        "Update Error", "Please Update Again");
            }

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (FirebaseInstanceId.getInstance().getToken() != null) {
            Log.e("Fire token ", FirebaseInstanceId.getInstance().getToken());
            ServerUtilities.register(this, FirebaseInstanceId.getInstance().getToken());
        }
    }

    public void locationDiable() {
        LocationManager lm = null;
        boolean gps_enabled = false, network_enabled = false;
        if (lm == null)
            lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            Builder dialog = new AlertDialog.Builder(HomepageActivity.this);
            dialog.setMessage("GPS is not Enable");
            dialog.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(
                                DialogInterface paramDialogInterface,
                                int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent(
                                    Settings.ACTION_SECURITY_SETTINGS);
                            startActivity(myIntent);
                            // get gps
                        }
                    });
            dialog.setNegativeButton("Close",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(
                                DialogInterface paramDialogInterface,
                                int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
            dialog.show();

        }
    }

    @Override
    public void initUI() {
        slideMenu = (SlideMenu) findViewById(R.id.slideMenu);
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public void clickFunction() {
        // TODO Auto-generated method stub
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                slideAction();
            }
        });

    }

    public void slideAction() {

        View contentView = inflater.inflate(
                R.layout.fragment_contentpageparent, null);

        slideMenu.addView(contentView, new SlideMenu.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT,
                LayoutParams.ROLE_CONTENT));

        secondaryMenu = inflater.inflate(R.layout.fragment_menuparent, null);

        slideMenu.addView(secondaryMenu, new SlideMenu.LayoutParams(300,
                LayoutParams.MATCH_PARENT, LayoutParams.ROLE_SECONDARY_MENU));

        // primaryMenu = inflater.inflate(R.layout.fragment_dataselectionparent,
        // null);
        // primaryMenu.setBackgroundColor(getResources().getColor(R.color.pink));
        // slideMenu.addView(primaryMenu, new SlideMenu.LayoutParams(300,
        // LayoutParams.MATCH_PARENT, LayoutParams.ROLE_PRIMARY_MENU));

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        i = i + 1;
        if (i == 1) {
            Toast.makeText(HomepageActivity.this, "Press again to exit",
                    Toast.LENGTH_SHORT).show();
        }

        if (i >= 2) {
            finish();
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                i = 0;

            }
        }, 2000);
    }

}
