package com.homesquad.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("message")
    @Expose
    public String message;
}