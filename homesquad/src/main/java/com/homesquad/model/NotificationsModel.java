package com.homesquad.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsModel {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("notifications")
    @Expose
    public List<Notification> notifications = null;
    @SerializedName("my_tablet")
    @Expose
    public String myTablet;
    @SerializedName("notification_count")
    @Expose
    public Integer notificationCount;

}