package com.homesquad.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings.Secure;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class URLUtil {

	public static SharedPreferences oSharedPreferences;
	public static Editor edit;
	public static Boolean LOGIN;

	public static String strPhoneIMEI;
	public static String strSynchronizeDate, strSynchronizeDateTomorrow,
			_strZoneName;
	public static Boolean isTodaySynchronize, istomorrowSynchronize,
			ismadeSynchronize;


	// public static final String BASEURL =  "http://booking.homesquad.ae:8090/homesquad_demo/driverappapi/";   //  DEMO

	 public static final String BASEURL =  "http://booking.homesquad.ae:8090/homesquad/driverappapi/";   //  LIVE


	// public static final String BASEURL =  "http://limshq.fortiddns.com:8095/homesquad/driverappapi/";   //




	public static void activateSharepreference(Context _context) {
		oSharedPreferences = _context.getSharedPreferences("userdetails",
				_context.MODE_PRIVATE);
		edit = oSharedPreferences.edit();

		strPhoneIMEI = Secure.getString(_context.getContentResolver(),Secure.ANDROID_ID);
		LOGIN = oSharedPreferences.getBoolean("login", false);
		strSynchronizeDate = oSharedPreferences.getString("date", "");

		strSynchronizeDateTomorrow = oSharedPreferences.getString("date2", "");
		_strZoneName = oSharedPreferences.getString("zonename", "");
		isTodaySynchronize = oSharedPreferences.getBoolean("todaysynchronize",
				false);
		istomorrowSynchronize = oSharedPreferences.getBoolean(
				"tomorrowsynchronize", false);
		ismadeSynchronize = oSharedPreferences.getBoolean("madesynchronize",
				false);

	}

	public static String getCurrentDate(Context _Context) {
		Calendar calendar = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(calendar.getTime());

		return formattedDate;
	}

	public static String Removenull(String URL1) {
		String URL = URL1.replace(" ", "%20").trim();
		System.out.println("**********" + URL);
		return URL;
	}

	public static String maidSynchronizeURL() {
		String URL = BASEURL + "getmaids?imei=" + strPhoneIMEI;
		// URL =
		// "http://www.emaid.info/mm/mm/api/getmaids?imei=356938035643809";

		return Removenull(URL);
	}

	public static String getLogin(String code) {

		String URL = BASEURL + "settings?imei=" + strPhoneIMEI + "&accesscode="
				+ code;
		// URL =
		// "http://www.emaid.info/mm/mm/api/settings?imei=356938035643809&accesscode=MMCC3412";

		return Removenull(URL);

	}

	public static String getCustomerdetails(String date) {

		String URL = BASEURL + "getschedule?imei=" + strPhoneIMEI + "&day="
				+ date;

		// URL =
		// "http://www.emaid.info/mm/mm/api/getschedule?imei=356938035643809&day="
		// +
		// date;
		return Removenull(URL);

	}

	public static String getMAidAttendance(String status, String maid_id) {

		String URL = BASEURL + "maidattendance?imei=" + strPhoneIMEI
				+ "&maid_id=" + maid_id + "&status=" + status;
		// URL =
		// "http://www.emaid.info/mm/mm/api/updatemaidstatus/?imei=356938035643809&maid_id="
		// + maid_id + "&status=" + status;
		return Removenull(URL);
	}

	public static String getCustomerAttendance(String status, String booking_id) {

		String URL = BASEURL + "updateservicestatus?imei=" + strPhoneIMEI
				+ "&booking_id=" + booking_id + "&status=" + status;
		// URL =
		// "http://www.emaid.info/mm/mm/api/updatebookingstatus/?imei=356938035643809&booking_id="
		// + booking_id + "&status=" + status;
		return Removenull(URL);
	}

	public static String getCustomerAttendance(String status,
			String booking_id, String payment, String amount, String method, String bill_no) {

		String URL = BASEURL + "updateservicestatus?imei=" + strPhoneIMEI
				+ "&booking_id=" + booking_id + "&status=" + status
				+ "&payment=" + payment + "&amount=" + amount + "&method="
				+ method +"&receipt_no="+ bill_no;
		// URL =
		// "http://www.emaid.info/mm/mm/api/updatebookingstatus/?imei=356938035643809&booking_id="
		// + booking_id + "&status=" + status;
		return Removenull(URL);
	}

	public static String getTransferMaid(String zone_id, String maid_id) {

		String URL = BASEURL + "transfermaid?imei=" + strPhoneIMEI
				+ "&maid_id=" + maid_id + "&zone_id=" + zone_id + "&day=1";
		// URL =
		// "http://www.emaid.info/mm/mm/api/transfermaid?imei=356938035643809&maid_id="
		// + maid_id + "&zone_id=" + zone_id;
		return Removenull(URL);
	}

	public static String getChangemaidRequest(String booking_id) {

		String URL = BASEURL + "maidchangerequest?imei=" + strPhoneIMEI
				+ "&booking_id=" + booking_id;

		return Removenull(URL);
	}

	public static String getLocationUpdateURl(String imei, String latitude,
			String longitude, String speed) {
		String URL = BASEURL + "tabletlocations?imei=" + imei + "&latitude="
				+ latitude + "&longitude=" + longitude + "&speed=" + speed;

		return Removenull(URL);
	}

	public static String getPaymentHistory() {
		String URL = BASEURL + "getpayments?imei=" + strPhoneIMEI;

		return Removenull(URL);
	}

	public static String getAttendanceReport() {
		String URL = BASEURL + "getmaidattendacereport?imei=" + strPhoneIMEI;

		return Removenull(URL);
	}

}
