package com.homesquad.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.homesquad.database.customer.SynchronizeCustomer;
import com.homesquad.driver.R;

public class CommonUtil
{
	public static Dialog dialog;

	public static void progressDialogue(Context _context)
	{
		dialog = new Dialog(_context);

		dialog.setContentView(R.layout.customdialogue_progress);

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog.setCancelable(false);

	}

	public static void CustomeDialogue(Context _context, String _strHeading, String _strMessage)
	{

		ActivityManager am = (ActivityManager) _context.getSystemService(_context.ACTIVITY_SERVICE);

		final Dialog dialog1 = new Dialog(_context);

		dialog1.setContentView(R.layout.customdialogue_error);

		dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog1.setCancelable(true);
		TextView textViewHeading = (TextView) dialog1.findViewById(R.id.customeerror_heading);
		TextView textViewContent = (TextView) dialog1.findViewById(R.id.customeerror_message);
		Button buttonOk = (Button) dialog1.findViewById(R.id.customeerror_btn);
		textViewHeading.setText(_strHeading.trim());
		textViewContent.setText(_strMessage.trim());

		buttonOk.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				dialog1.dismiss();
			}
		});

		dialog1.show();

	}

	public static void synchrinoouserror(final Activity _context, String _strHeading, String _strMessage)
	{

		ActivityManager am = (ActivityManager) _context.getSystemService(_context.ACTIVITY_SERVICE);

		final Dialog dialog1 = new Dialog(_context);

		dialog1.setContentView(R.layout.customdialogue_error);

		dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog1.setCancelable(true);
		TextView textViewHeading = (TextView) dialog1.findViewById(R.id.customeerror_heading);
		TextView textViewContent = (TextView) dialog1.findViewById(R.id.customeerror_message);
		Button buttonOk = (Button) dialog1.findViewById(R.id.customeerror_btn);
		textViewHeading.setText(_strHeading.trim());
		textViewContent.setText(_strMessage.trim());

		buttonOk.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				
				SynchronizeCustomer.diologueSynchronize(_context, "1");
			
				dialog1.dismiss();
			}
		});

		dialog1.show();

	}

}
