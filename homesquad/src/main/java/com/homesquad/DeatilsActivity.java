package com.homesquad;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.emaid.azinova.EmaidLogin;
import com.emaid.azinova.EmaidLoginContainer;
import com.google.gson.Gson;
import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.database.maid.ImageLoderutil;
import com.homesquad.database.maid.MySQLiteHelper;
import com.homesquad.driver.R;
import com.homesquad.services.CustomerattendanceServices;
import com.homesquad.services.CustomerattendancepaymentServices;
import com.homesquad.services.MaidChangeServices;
import com.homesquad.services.MaidattendanceServices;
import com.homesquad.services.TranfermaidServices;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import model.EmaidCustomerDetailsURLContainerNew;

public class DeatilsActivity extends BaseActivity implements BaseInterface {
    MySQLiteHelper mySQLiteHelper;
    Button _buttonShiftTime, _buttonMaidName, _buttonMaidCountry,
            _buttonShiftStart, _buttonShiftEnd, _buttonCustomerName,
            _buttonMobileNumber, _buttonAddress, _buttonArea, _buttonKey,
            _buttonNote, _buttonAmount, _buttonCustomerType;
    Button _buttonMaidChange, _buttonMaidAttendance, _buttonTransfermaid,
            _buttonCustomerAttendance, _buttonMaidAttendance_out;
    TextView maid_status;
    LinearLayout maid_status_layout;

    LinearLayout callButton, locationButton;

    ImageView _imageViewPhoto;
    ProgressBar _progressBarPhoto;
    String _strBookingid = "";
    int intDate;
    Dialog dialogzoneselection, dialog2;
    List<EmaidCustomerDetailsURLContainerNew> containers = new ArrayList<EmaidCustomerDetailsURLContainerNew>();
    LinearLayout _lytBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todaydetails);
        GlobalManager.getdata().setCurrentActivity("DeatilsActivity");
        initUI();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void initUI() {

        mySQLiteHelper = new MySQLiteHelper(DeatilsActivity.this);
        _buttonShiftTime = (Button) findViewById(R.id.todaydeatils_btn_shift);
        _buttonMaidName = (Button) findViewById(R.id.todaydeatils_btn_maidname);
        _buttonMaidCountry = (Button) findViewById(R.id.todaydeatils_btn_maidcountry);
        _buttonShiftStart = (Button) findViewById(R.id.todaydeatils_btn_shiftstart);
        _buttonShiftEnd = (Button) findViewById(R.id.todaydeatils_btn_shiftend);
        _buttonCustomerName = (Button) findViewById(R.id.todaydeatils_btn_customername);
        _buttonMobileNumber = (Button) findViewById(R.id.todaydeatils_btn_customerphoneno);
        _buttonAddress = (Button) findViewById(R.id.todaydeatils_btn_customeraddress);
        _buttonArea = (Button) findViewById(R.id.todaydeatils_btn_customerareazone);
        _buttonKey = (Button) findViewById(R.id.todaydeatils_btn_customerkey);
        _buttonNote = (Button) findViewById(R.id.todaydeatils_btn_customernote);
        _buttonAmount = (Button) findViewById(R.id.todaydeatils_btn_amount);  // Leo
        _buttonCustomerType = (Button) findViewById(R.id.todaydeatils_btn_customertype);  // Leo

        _buttonMaidChange = (Button) findViewById(R.id.todaydeatils_btn_maidchange);
        _buttonMaidAttendance = (Button) findViewById(R.id.todaydeatils_btn_maidattendance);
        _buttonMaidAttendance_out = findViewById(R.id.todaydeatils_btn_maidattendance_out);
        _buttonTransfermaid = (Button) findViewById(R.id.todaydeatils_btn_tranfermaid);
        _buttonCustomerAttendance = (Button) findViewById(R.id.todaydeatils_btn_customerattendance);

        _imageViewPhoto = (ImageView) findViewById(R.id.todaydeatils_iv_photo);
        _progressBarPhoto = (ProgressBar) findViewById(R.id.todaydeatils_progress_photo);

        _lytBack = (LinearLayout) findViewById(R.id.todaydeatils_lyt_back);
        maid_status = (TextView) findViewById(R.id.maid_status);
        maid_status_layout = (LinearLayout) findViewById(R.id.maid_status_layout);

        callButton = findViewById(R.id.callButton);
        locationButton = findViewById(R.id.locationButton);

        _strBookingid = getIntent().getExtras().getString("bookingid");
        intDate = getIntent().getExtras().getInt("date");
        clickFunction();

    }

    @Override
    public void clickFunction() {

        initValidationBinding();

        Log.e("Location", containers.get(0).getCustomer_latitude() + containers.get(0).getCustomer_longitude());
        callButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//                sendSMS(containers.get(0).getCustomer_mobile(),"");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + containers.get(0).getCustomer_mobile()));
                startActivity(intent);
            }
        });

        locationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (containers.get(0).getCustomer_latitude().isEmpty() && containers.get(0).getCustomer_address().isEmpty()) {

                        Toast.makeText(DeatilsActivity.this, "Location information not available", Toast.LENGTH_SHORT).show();

//                        Log.e("Location", containers.get(0).getCustomer_latitude() + containers.get(0).getCustomer_longitude());

                    } else {
                        Uri gmmIntentUri;

                        if (containers.get(0).getCustomer_latitude().isEmpty()) {
                            gmmIntentUri = Uri.parse("google.navigation:q=" + containers.get(0).getApartment_number() + ", "
                                    + containers.get(0).getArea() + ", "
                                    + containers.get(0).getCustomer_address());
                        } else {
                            gmmIntentUri = Uri.parse("google.navigation:q=" + containers.get(0).getCustomer_latitude() + "," + containers.get(0).getCustomer_longitude());
                        }


                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");

                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        } else {
                            Toast.makeText(DeatilsActivity.this, "Google Maps not available, Please install from play store.", Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (Exception e) {

                }

            }
        });

        _buttonMaidChange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                maidChangeDialogue();

            }
        });

        _buttonMaidAttendance.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (containers.get(0).getMaid_status().equalsIgnoreCase("0")) {
                    confirmationDialogue(0);
                } else if (containers.get(0).getMaid_status()
                        .equalsIgnoreCase("1")) {
                    confirmationDialogue(1);
                } else {
                    confirmationDialogue(0);
                }

            }
        });


        _buttonMaidAttendance_out.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (containers.get(0).getMaid_status().equalsIgnoreCase("0")) {
                    confirmationDialogue(0);
                } else if (containers.get(0).getMaid_status()
                        .equalsIgnoreCase("1")) {
                    confirmationDialogue(1);
                } else {
                    confirmationDialogue(0);
                }

            }
        });

        _buttonCustomerAttendance.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (containers.get(0).getService_status().equalsIgnoreCase("0")) {
                    confirmationDialogue(2);
                } else if (containers.get(0).getService_status()
                        .equalsIgnoreCase("1")) {
                    // confirmationDialogue(3);
                    paymentDialogue();
                }

            }
        });

        _buttonTransfermaid.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // confirmationDialogue(4);
                zoneselection();

            }
        });

        _lytBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(DeatilsActivity.this,
                        HomepageActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void sendSMS(String number, String message) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", message);
        startActivity(it);
    }


    public void initValidationBinding() {
        containers.clear();
        if (intDate == 2) {
            containers = mySQLiteHelper
                    .returnSinglecustomerTomorrow(_strBookingid);
        } else {
            containers = mySQLiteHelper.returnSinglecustomer(_strBookingid);
        }

        if (containers.isEmpty()) {
            return;
        }

        _buttonShiftTime.setText(hr12Format(containers.get(0).getShift_start())
                + " - " + hr12Format(containers.get(0).getShift_end()));
        _buttonMaidName.setText(containers.get(0).getMaid_name());
        _buttonMaidCountry.setText(containers.get(0).getMaid_country());
        _buttonShiftStart
                .setText(hr12Format(containers.get(0).getShift_start()));
        _buttonShiftEnd.setText(hr12Format(containers.get(0).getShift_end()));
        _buttonCustomerName.setText(containers.get(0).getCustomer_name());
        _buttonMobileNumber.setText(containers.get(0).getCustomer_mobile());
        _buttonAddress.setText(containers.get(0).getCustomer_address());
        _buttonArea.setText(containers.get(0).getArea());
        Log.e("Fee", containers.get(0).toString());

        _buttonCustomerType.setText(containers.get(0).getCustomer_type());

        if (containers.get(0).getService_fee().equalsIgnoreCase("CC")) {
            _buttonAmount.setText("Amount  : " + containers.get(0).getService_fee() + "      " + "|   Cleaning Materials   : " + containers.get(0).getCleaning_material());
        } else {
            _buttonAmount.setText("Amount  : " + containers.get(0).getService_fee() + " AED  " + "|   Cleaning Materials   : " + containers.get(0).getCleaning_material());
        }


        if (containers.get(0).getKey_status().equals("1")) {
            _buttonKey.setText("Yes");
        } else {
            _buttonKey.setText("No");
        }

        _buttonNote.setText(containers.get(0).getBooking_note());
        //_buttonNote.setText(containers.get(0).getMaid_status());

        ImageLoderutil.imageBinding(DeatilsActivity.this, containers.get(0)
                .getMaid_id(), _imageViewPhoto, _progressBarPhoto);

        System.out.println("****33333333***"
                + containers.get(0).getMaid_status());
        System.out.println("****44444444*****"
                + containers.get(0).getService_status());

        if (intDate == 2) {

            _buttonMaidAttendance.setBackgroundDrawable(getResources()
                    .getDrawable(R.drawable.maidout_disable));
            _buttonMaidChange.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.maidchange_disable));
            _buttonTransfermaid.setBackgroundDrawable(getResources()
                    .getDrawable(R.drawable.transfermaid_disable));
            _buttonCustomerAttendance.setBackgroundDrawable(getResources()
                    .getDrawable(R.drawable.customerin_disable));
            _buttonMaidAttendance.setEnabled(false);
            _buttonMaidChange.setEnabled(false);
            _buttonTransfermaid.setEnabled(false);
            _buttonCustomerAttendance.setEnabled(false);
            _buttonMaidAttendance_out.setEnabled(false);
        } else {
            if (containers.get(0).getMaid_status().equalsIgnoreCase("0")) {
//				
                maid_status.setText("NOT IN VEHICLE");
                maid_status_layout.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.pending_bg));

                _buttonMaidAttendance.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidin));
                _buttonMaidAttendance_out.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidout_disbaled));

                _buttonMaidAttendance_out.setEnabled(false);
                _buttonMaidAttendance.setEnabled(true);

                _buttonMaidChange.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidchange_disable));
                _buttonTransfermaid.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.transfermaid));
                _buttonCustomerAttendance.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.customerin_disable));
                //   _buttonMaidAttendance.setEnabled(true);
                _buttonMaidChange.setEnabled(false);
                _buttonTransfermaid.setEnabled(true);
                _buttonCustomerAttendance.setEnabled(false);

            } else if ((containers.get(0).getMaid_status()
                    .equalsIgnoreCase("2"))) {
//				maid_status.setText("IN VEHICLE");
//				_buttonMaidAttendance.setBackgroundDrawable(getResources()
//						.getDrawable(R.drawable.maidin));

                _buttonMaidAttendance.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidin));

                _buttonMaidAttendance_out.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidout_disbaled));

                _buttonMaidChange.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidchange_disable));

                _buttonMaidAttendance_out.setEnabled(false);

                _buttonMaidAttendance.setEnabled(true);

                maid_status.setText("NOT IN VEHICLE");
                maid_status_layout.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.pending_bg));

                _buttonCustomerAttendance.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.customerin_disable));

                //  _buttonMaidAttendance.setEnabled(true);
                _buttonMaidChange.setEnabled(false);

                _buttonCustomerAttendance.setEnabled(false);
                if (containers.get(0).getService_status().equalsIgnoreCase("2")
                        || containers.get(0).getService_status()
                        .equalsIgnoreCase("3")) {
                    _buttonTransfermaid.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.transfermaid_disable));
                    _buttonTransfermaid.setEnabled(false);
                } else {
                    _buttonTransfermaid.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.transfermaid));
                    _buttonTransfermaid.setEnabled(true);
                }

            } else {

//				_buttonMaidAttendance.setBackgroundDrawable(getResources()
//						.getDrawable(R.drawable.maidoutneww));

                _buttonMaidAttendance.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidin_disabled));
                _buttonMaidAttendance_out.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidoutneww));

                _buttonMaidAttendance_out.setEnabled(true);
                _buttonMaidAttendance.setEnabled(false);

                _buttonMaidChange.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.maidchange));
                _buttonTransfermaid.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.transfermaid));
                _buttonCustomerAttendance.setBackgroundDrawable(getResources()
                        .getDrawable(R.drawable.customerin));
                //_buttonMaidAttendance.setEnabled(true);
                _buttonMaidChange.setEnabled(true);
                _buttonTransfermaid.setEnabled(true);
                _buttonCustomerAttendance.setEnabled(true);

                if (containers.get(0).getService_status().equalsIgnoreCase("1")) {

                    _buttonMaidChange.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.maidchange_disable));
                    _buttonTransfermaid.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.transfermaid));
                    _buttonCustomerAttendance
                            .setBackgroundDrawable(getResources().getDrawable(
                                    R.drawable.customerout));
                    _buttonMaidChange.setEnabled(false);
                    _buttonTransfermaid.setEnabled(true);
                    _buttonCustomerAttendance.setEnabled(true);

                    maid_status.setText("ON DUTY");
                    maid_status_layout.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.onduty_bg));

                } else if (containers.get(0).getService_status()
                        .equalsIgnoreCase("2")
                        || containers.get(0).getService_status()
                        .equalsIgnoreCase("3")) {

                    maid_status.setText("COMPLETED");
                    maid_status_layout.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.completed_bg));

                    _buttonMaidChange.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.maidchange_disable));

                    _buttonTransfermaid.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.transfermaid_disable));
                    _buttonCustomerAttendance
                            .setBackgroundDrawable(getResources().getDrawable(
                                    R.drawable.customerout_disable));
                    _buttonMaidChange.setEnabled(false);
                    _buttonTransfermaid.setEnabled(false);
                    _buttonCustomerAttendance.setEnabled(false);
                } else {
                    maid_status.setText("IN VEHICLE");
                    maid_status_layout.setBackgroundDrawable(getResources()
                            .getDrawable(R.drawable.pending_bg));
                }


            }

        }
    }

    public String hr12Format(String time1) {

        String Time = "";
        try {
            String _24HourTime = time1;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            java.util.Date _24HourDt = _24HourSDF.parse(_24HourTime);

            Time = _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
            Time = "00:00 AM";
        }

        return Time;
    }

    public void zoneselection() {
        dialogzoneselection = new Dialog(DeatilsActivity.this);

        dialogzoneselection
                .setContentView(R.layout.customdialogue_zoneselection);

        dialogzoneselection.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialogzoneselection.setCancelable(true);

        GridView gridViewZone = (GridView) dialogzoneselection
                .findViewById(R.id.customdialoguezoneselection_grid);
        GridviewAdapter adapter = new GridviewAdapter();
        gridViewZone.setAdapter(adapter);

        dialogzoneselection.show();
    }

    public List<EmaidLoginContainer> getZonesList() {
        List<EmaidLoginContainer> zonesArray = new ArrayList<EmaidLoginContainer>();
        zonesArray.clear();
        String strzone = URLUtil.oSharedPreferences.getString("zone", "");

        try {
            Gson gson = new Gson();
            EmaidLogin emaidLogin = gson.fromJson(strzone, EmaidLogin.class);
            zonesArray = emaidLogin.getZones();

            for (int i = 0; i < zonesArray.size(); i++) {
                if (URLUtil._strZoneName.equalsIgnoreCase(zonesArray.get(i)
                        .getZone_name())) {
                    zonesArray.remove(i);
                }

            }

        } catch (Exception e) {

        }

        return zonesArray;

    }

    public class GridviewAdapter extends BaseAdapter {
        LayoutInflater inflater;
        List<EmaidLoginContainer> zonesArrays = new ArrayList<EmaidLoginContainer>();

        public GridviewAdapter() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            zonesArrays.clear();
            zonesArrays = getZonesList();

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return zonesArrays.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            // TODO Auto-generated method stub
            View view = inflater.inflate(R.layout.adapter_zonesinglecell, null);
            TextView textView = (TextView) view
                    .findViewById(R.id.adapterzonelist_txt);
            textView.setText(zonesArrays.get(arg0).getZone_name());
            view.setTag(zonesArrays.get(arg0).getZone_id());
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    confirmationDialogueTransfermaid(v.getTag().toString());

                }
            });
            return view;
        }

    }

    public void confirmationDialogue(final int id) {

        final Dialog dialog1 = new Dialog(DeatilsActivity.this);

        dialog1.setContentView(R.layout.customdialogue_confirmation);

        dialog1.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog1.setCancelable(true);
        TextView textViewHeading = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_heading);
        TextView textViewContent = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_message);
        Button buttonYes = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_yes);
        Button buttonNo = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_no);
        textViewHeading.setText("Confirmation");
        textViewContent.setText("Do you Want to continue ?");

        buttonYes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // id - 0 - maid in ,1- maid out ,2 -customer in ,3-customer

                switch (id) {
                    case 0:
                        mySQLiteHelper.setMaidAttendance(containers.get(0)
                                .getMaid_id(), "1");

                        Intent i = new Intent(DeatilsActivity.this,
                                MaidattendanceServices.class);
                        i.putExtra("attendance", "1");

                        i.putExtra("maidid", containers.get(0).getMaid_id());

                        startService(i);
                        initValidationBinding();
                        dialog1.dismiss();
                        break;
                    case 1:
                        mySQLiteHelper.setMaidAttendance(containers.get(0)
                                .getMaid_id(), "2");

                        Intent i1 = new Intent(DeatilsActivity.this,
                                MaidattendanceServices.class);
                        i1.putExtra("attendance", "2");
                        i1.putExtra("maidid", containers.get(0).getMaid_id());

                        startService(i1);
                        dialog1.dismiss();
                        Intent intent33 = new Intent(DeatilsActivity.this,
                                HomepageActivity.class);
                        startActivity(intent33);
                        finish();

                        break;
                    case 2:

                        mySQLiteHelper.setCustomerAttendance(containers.get(0)
                                .getBooking_id(), "1");

                        Intent i2 = new Intent(DeatilsActivity.this,
                                CustomerattendanceServices.class);
                        i2.putExtra("attendance", "1");

                        i2.putExtra("bookingid", containers.get(0).getBooking_id());

                        startService(i2);
                        dialog1.dismiss();
                        Intent intent22 = new Intent(DeatilsActivity.this,
                                HomepageActivity.class);
                        startActivity(intent22);
                        finish();

                        break;

                    default:
                        break;
                }

            }
        });
        buttonNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog1.dismiss();

            }
        });

        dialog1.show();

    }

    public void confirmationDialogueTransfermaid(final String zoneid) {

        final Dialog dialog1 = new Dialog(DeatilsActivity.this);

        dialog1.setContentView(R.layout.customdialogue_confirmation);

        dialog1.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog1.setCancelable(true);
        TextView textViewHeading = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_heading);
        TextView textViewContent = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_message);
        Button buttonYes = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_yes);
        Button buttonNo = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_no);
        textViewHeading.setText("Confirmation");
        textViewContent.setText("Do you Want to continue ?");

        buttonYes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // id - 0 - maid in ,1- maid out ,2 -customer in ,3-customer

                mySQLiteHelper.setTransfermaid(containers.get(0).getMaid_id());

                Intent i = new Intent(DeatilsActivity.this,
                        TranfermaidServices.class);
                i.putExtra("zoneid", zoneid);

                i.putExtra("maidid", containers.get(0).getMaid_id());
                startService(i);

                dialogzoneselection.dismiss();
                dialog1.dismiss();
                Intent intent = new Intent(DeatilsActivity.this,
                        HomepageActivity.class);
                startActivity(intent);
                finish();

            }
        });
        buttonNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog1.dismiss();

            }
        });

        dialog1.show();

    }

    public void paymentDialogue() {

        dialog2 = new Dialog(DeatilsActivity.this);

        dialog2.setContentView(R.layout.customdialogue_payment);

        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog2.setCancelable(true);
        final EditText editTextAmount = (EditText) dialog2
                .findViewById(R.id.customdialoguepayment_txt_amount);
        final EditText editBillNumber = (EditText) dialog2
                .findViewById(R.id.customdialoguepayment_bill_no);

        Button buttonPaymentReceived = (Button) dialog2
                .findViewById(R.id.customdialoguepayment_btn_paymentreceived);
        Button buttonPaymentNotReceived = (Button) dialog2
                .findViewById(R.id.customdialoguepayment_btn_paymentnotreceived);
        Button buttonServiceNotdone = (Button) dialog2
                .findViewById(R.id.customdialoguepayment_btn_servicenotdone);

        buttonPaymentReceived.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (editTextAmount.getText().toString().trim().equals("")) {
                    CommonUtil.CustomeDialogue(DeatilsActivity.this,
                            "Invalid Input", "Please Enter the Amount");
                } else if (editBillNumber.getText().toString().trim().equals("")) {
                    CommonUtil.CustomeDialogue(DeatilsActivity.this,
                            "Invalid Input", "Please Enter the Bill Number");
                } else {
                    paymenttypeOption(1, editTextAmount.getText().toString()
                            .trim(), "1", editBillNumber.getText().toString()
                            .trim());
                }

            }
        });
        buttonPaymentNotReceived.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                confirmationCustomerAttendance(2, "", "0");

            }
        });
        buttonServiceNotdone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                confirmationCustomerAttendance(3, "", "");

            }
        });

        dialog2.show();
    }

    public void paymenttypeOption(final int key, final String amount,
                                  final String payment, final String bill_no) {
        final Dialog dialogPaymenttype = new Dialog(DeatilsActivity.this);

        dialogPaymenttype.setContentView(R.layout.customdialogue_paymenttype);

        dialogPaymenttype.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final RadioButton radioCard = (RadioButton) dialogPaymenttype
                .findViewById(R.id.customdialoguepaymentstype_radio_card);
        final RadioButton radioCash = (RadioButton) dialogPaymenttype
                .findViewById(R.id.customdialoguepaymentstype_radio_cash);
        Button buttonYes = (Button) dialogPaymenttype
                .findViewById(R.id.customdialoguepaymentstype_btn_yes);
        Button buttonNo = (Button) dialogPaymenttype
                .findViewById(R.id.customdialoguepaymentstype_btn_no);

        radioCash.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                radioCash.setChecked(true);
                radioCard.setChecked(false);

            }
        });
        radioCard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                radioCash.setChecked(false);
                radioCard.setChecked(true);

            }
        });

        dialogPaymenttype.setCancelable(true);

        buttonYes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                String type = "0";// 0- cash 1 for card
                if (radioCard.isChecked()) {
                    type = "1";
                } else {
                    type = "0";
                }

                mySQLiteHelper.setCustomerAttendance(containers.get(0)
                        .getBooking_id(), "2");

                Intent i1 = new Intent(DeatilsActivity.this,
                        CustomerattendancepaymentServices.class);
                i1.putExtra("attendance", "2");
                i1.putExtra("payment", payment);
                i1.putExtra("amount", amount);
                i1.putExtra("type", type);
                i1.putExtra("bill", bill_no);

                i1.putExtra("bookingid", containers.get(0).getBooking_id());
                startService(i1);

                initValidationBinding();
                dialog2.cancel();
                dialogPaymenttype.dismiss();
                Intent intent = new Intent(DeatilsActivity.this,
                        HomepageActivity.class);
                startActivity(intent);
                finish();

            }
        });

        buttonNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                dialogPaymenttype.dismiss();

            }
        });
        dialogPaymenttype.show();
    }

    public void confirmationCustomerAttendance(final int key,
                                               final String amount, final String payment) {

        final Dialog dialog1 = new Dialog(DeatilsActivity.this);

        dialog1.setContentView(R.layout.customdialogue_confirmation);

        dialog1.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog1.setCancelable(true);
        TextView textViewHeading = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_heading);
        TextView textViewContent = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_message);
        Button buttonYes = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_yes);
        Button buttonNo = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_no);
        textViewHeading.setText("Confirmation");
        textViewContent.setText("Do you Want to continue ?");

        buttonYes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                switch (key) {

                    case 2:

                        mySQLiteHelper.setCustomerAttendance(containers.get(0)
                                .getBooking_id(), "2");

                        Intent i2 = new Intent(DeatilsActivity.this,
                                CustomerattendancepaymentServices.class);
                        i2.putExtra("attendance", "2");
                        i2.putExtra("payment", payment);
                        i2.putExtra("amount", amount);
                        i2.putExtra("type", "");
                        i2.putExtra("bookingid", containers.get(0).getBooking_id());
                        startService(i2);

                        break;
                    case 3:

                        mySQLiteHelper.setCustomerAttendance(containers.get(0)
                                .getBooking_id(), "3");

                        Intent i3 = new Intent(DeatilsActivity.this,
                                CustomerattendanceServices.class);
                        i3.putExtra("attendance", "3");

                        i3.putExtra("bookingid", containers.get(0).getBooking_id());
                        startService(i3);

                        break;

                    default:
                        break;
                }
                initValidationBinding();
                dialog2.cancel();
                dialog1.dismiss();
                Intent intent = new Intent(DeatilsActivity.this,
                        HomepageActivity.class);
                startActivity(intent);
                finish();

            }
        });
        buttonNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                dialog1.dismiss();

            }
        });

        dialog1.show();

    }

    // ***************maid change otion ******************
    public void maidChangeDialogue() {
        final Dialog dialog1 = new Dialog(DeatilsActivity.this);

        dialog1.setContentView(R.layout.customdialogue_confirmation);

        dialog1.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog1.setCancelable(true);
        TextView textViewHeading = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_heading);
        TextView textViewContent = (TextView) dialog1
                .findViewById(R.id.customdialoguecofirmation_message);
        Button buttonYes = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_yes);
        Button buttonNo = (Button) dialog1
                .findViewById(R.id.customdialoguecofirmation_btn_no);
        textViewHeading.setText("Maid Change Confirmation");
        textViewContent.setText("Do you Want to continue ?");

        buttonYes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent i3 = new Intent(DeatilsActivity.this,
                        MaidChangeServices.class);

                i3.putExtra("bookingid", containers.get(0).getBooking_id());
                startService(i3);
                dialog1.dismiss();

                Intent intent = new Intent(DeatilsActivity.this,
                        HomepageActivity.class);
                startActivity(intent);
                finish();
            }
        });
        buttonNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                dialog1.dismiss();
            }
        });

        dialog1.show();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Intent intent = new Intent(DeatilsActivity.this, HomepageActivity.class);
        startActivity(intent);
        finish();
    }

}
