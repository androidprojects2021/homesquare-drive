package com.homesquad;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.database.customer.SynchronizeCustomer;
import com.homesquad.database.maid.ImageLoderutil;
import com.homesquad.database.maid.MySQLiteHelper;
import com.homesquad.driver.R;
import com.homesquad.utils.URLUtil;

public class SplashScreen extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    MySQLiteHelper mySQLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        mySQLiteHelper = new MySQLiteHelper(SplashScreen.this);
        mySQLiteHelper.getWritableDatabase();
        GlobalManager.getdata().setCurrentActivity("SplashScreen");
        URLUtil.activateSharepreference(SplashScreen.this);
        //	LocationAlarm.initAlarm(SplashScreen.this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (URLUtil.oSharedPreferences.getBoolean("login", false)) {

                    ImageLoderutil.retriveMaid(SplashScreen.this);
                    if (!SynchronizeCustomer.retriveCustomer(SplashScreen.this)
                            && (!ImageLoderutil.isSynchronize())) {
                        Intent oIntent = new Intent(SplashScreen.this,
                                Synchronize.class);
                        oIntent.putExtra("page", "0");
                        startActivity(oIntent);
                        finish();
                        overridePendingTransition(R.anim.slide_out_front,
                                R.anim.slide_in_front);

                    } else if (!URLUtil.strSynchronizeDate
                            .equalsIgnoreCase(URLUtil
                                    .getCurrentDate(SplashScreen.this))) {
                        Intent oIntent = new Intent(SplashScreen.this,
                                Synchronize.class);
                        oIntent.putExtra("page", "0");
                        startActivity(oIntent);
                        finish();
                        overridePendingTransition(R.anim.slide_out_front,
                                R.anim.slide_in_front);
                    } else {
                        Intent oIntent = new Intent(SplashScreen.this,
                                HomepageActivity.class);

                        if (getIntent().getExtras() != null) {

                         //   Log.e("data", getIntent().getExtras().getString("body"));

                            if (getIntent().getExtras().getString("body") != null) {

                                oIntent.putExtra("message", getIntent().getExtras().getString("body"));
                            }
                        }

                        startActivity(oIntent);

                        finish();
                        overridePendingTransition(R.anim.slide_out_front,
                                R.anim.slide_in_front);
                    }

                } else {
                    Intent oIntent = new Intent(SplashScreen.this, Login.class);
                    startActivity(oIntent);
                    finish();
                    overridePendingTransition(R.anim.slide_out_front,
                            R.anim.slide_in_front);
                }
            }

        }, SPLASH_TIME_OUT);

    }

}
