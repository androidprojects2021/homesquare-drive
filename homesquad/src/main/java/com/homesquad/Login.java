package com.homesquad;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.emaid.azinova.EmaidLogin;
import com.google.gson.Gson;
import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.database.customer.SynchronizeCustomer;
import com.homesquad.database.maid.ImageLoderutil;
import com.homesquad.driver.R;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class Login extends BaseActivity implements BaseInterface
{
	Button _btnSubmit;
	EditText _etxtCode;
	ImageView _ivEMEI;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		GlobalManager.getdata().setCurrentActivity("Login");
		initUI();
		clickFunction();
	}

	@Override
	public void initUI()
	{
		_btnSubmit = (Button) findViewById(R.id.login_btn_submit001);
		_etxtCode = (EditText) findViewById(R.id.login_etxt_code);
		_ivEMEI = (ImageView) findViewById(R.id.login_ivchar);
		

	}

	@Override
	public void clickFunction()
	{

		_btnSubmit.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				if(!_etxtCode.getText().toString().trim().isEmpty()){
					getData(URLUtil.getLogin(_etxtCode.getText().toString().trim()));
				}else{
					CommonUtil.CustomeDialogue(Login.this, "Error", "Invalid Login");
				}
				

			}
		});

		_ivEMEI.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				CommonUtil.CustomeDialogue(Login.this, "IMEI NUMBER", URLUtil.strPhoneIMEI);
			}
		});

	}

	public void getData(String URL)
	{
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(30000);
		client.get(URL, new AsyncHttpResponseHandler()
		{
			@Override
			public void onStart()
			{
				// TODO Auto-generated method stub
				super.onStart();
				CommonUtil.progressDialogue(Login.this);
				CommonUtil.dialog.show();
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);
				System.out.println("*******"+arg0);
				URLUtil.edit.putBoolean("login", false);
				URLUtil.edit.commit();
				CommonUtil.CustomeDialogue(Login.this, "Error", "Network Error");

			}

			@Override
			public void onSuccess(int arg0, String arg1)
			{
				// TODO Auto-generated method stub
				super.onSuccess(arg0, arg1);
				try
				{

					Gson gson = new Gson();
					EmaidLogin oEmaidLogin = gson.fromJson(arg1, EmaidLogin.class);

					if (oEmaidLogin.getStatus().equals("success"))
					{

						for (int i = 0; i < oEmaidLogin.getZones().size(); i++)
						{
							if (oEmaidLogin.getZones().get(i).getZone_id().equalsIgnoreCase(oEmaidLogin.getMy_zone()))
							{

								URLUtil.edit.putString("zonename", oEmaidLogin.getZones().get(i).getZone_name() +" - "+ oEmaidLogin.getZones().get(i).getDriver_name());
							}

						}

//						URLUtil.edit.putString("tabletid", oEmaidLogin.getMy_tablet());
						
						URLUtil.edit.putString("zone", arg1);
						URLUtil.edit.putBoolean("login", true);
						URLUtil.edit.commit();
						URLUtil.activateSharepreference(Login.this);

						ImageLoderutil.retriveMaid(Login.this);
						if (!SynchronizeCustomer.retriveCustomer(Login.this) && (!ImageLoderutil.isSynchronize()))
						{
							Intent oIntent = new Intent(Login.this, Synchronize.class);
							oIntent.putExtra("page", "0");
							startActivity(oIntent);

							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);

						}
						else if (URLUtil.strSynchronizeDate.equalsIgnoreCase(""))
						{
							Intent oIntent = new Intent(Login.this, Synchronize.class);
							oIntent.putExtra("page", "0");
							startActivity(oIntent);
							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
						}
						else
						{
							Intent oIntent = new Intent(Login.this, HomepageActivity.class);

							startActivity(oIntent);

							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
						}

					}
					else
					{
						URLUtil.edit.putBoolean("login", false);
						URLUtil.edit.commit();
						CommonUtil.CustomeDialogue(Login.this, "Error", oEmaidLogin.getStatus());
					}

				}
				catch (Exception e)
				{
					URLUtil.edit.putBoolean("login", false);
					URLUtil.edit.commit();
					CommonUtil.CustomeDialogue(Login.this, "Error", "Network Error");
				}

			}

			@Override
			public void onFinish()
			{
				// TODO Auto-generated method stub
				super.onFinish();
				CommonUtil.dialog.dismiss();

			}

		});
	}

}
