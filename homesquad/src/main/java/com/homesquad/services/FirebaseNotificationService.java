package com.homesquad.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.homesquad.BaseActivity;
import com.homesquad.HomepageActivity;
import com.homesquad.Synchronize;
import com.homesquad.driver.R;
import com.homesquad.notification.Myservices;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Leo elstin on 8/6/18.
 */

public class FirebaseNotificationService extends FirebaseMessagingService {

    private static final String TAG = "Firebase";

    public FirebaseNotificationService() {
        super();

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

//        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Log.e(" Error ", remoteMessage.getData().toString());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                try {
                    generateNotification(this, remoteMessage.getNotification().getBody());
                } catch (Exception e) {

//                Log.e(" Error ", e.getMessage());
                }

            }
        }


    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private static void generateNotification(final Context context, String message) {

        try {

            if (BaseActivity.appInFront) {
                System.out.println("*****cccc**");

                try {
                    playsound(context);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Intent myIntent = new Intent(context, Myservices.class);
                myIntent.putExtra("message", message);
                myIntent.putExtra("heading", "Notification");
                myIntent.putExtra("isNotification", "isNotificationfsfsfs");
                context.startService(myIntent);

            } else {
                int notification_id = 0;
                Calendar c = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("kkmmss");
                String strTodayDate = df.format(c.getTime());

                try {
                    notification_id = Integer.parseInt(strTodayDate);
                } catch (Exception e) {
                    notification_id = 0;
                }

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher).setContentTitle(message).setContentText("");
                mBuilder.setAutoCancel(true);
                Intent resultIntent = new Intent(context, HomepageActivity.class);

                resultIntent.putExtra("page", "notification");

                // Add the bundle to the intent

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

                stackBuilder.addParentStack(Synchronize.class);

                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(resultPendingIntent);
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder.setSound(alarmSound);
                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(notification_id, mBuilder.build());
            }

        } catch (Exception e) {
            int notification_id = 0;
            Calendar c = Calendar.getInstance();

            SimpleDateFormat df = new SimpleDateFormat("kkmmss");
            String strTodayDate = df.format(c.getTime());

            try {
                notification_id = Integer.parseInt(strTodayDate);
            } catch (Exception es) {
                notification_id = 0;
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher).setContentTitle(message).setContentText("");
            mBuilder.setAutoCancel(true);
            Intent resultIntent = new Intent(context, HomepageActivity.class);

            resultIntent.putExtra("page", "notification");

            // Add the bundle to the intent

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                stackBuilder.addParentStack(Synchronize.class);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                stackBuilder.addNextIntent(resultIntent);
            }
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(alarmSound);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (mNotificationManager != null) {
                mNotificationManager.notify(notification_id, mBuilder.build());
            }
        }

    }

    public static void playsound(Context context) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        RingtoneManager.getRingtone(context, alarmSound).play();

    }
}
