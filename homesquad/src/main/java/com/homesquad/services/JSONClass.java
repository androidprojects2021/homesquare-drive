package com.homesquad.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class JSONClass {
	static String result = "";
	static JSONObject jArray = null;

	public static String getJSONfromURL(String url) {
		
		System.out.println("*******getJSONfromURL********"+ url);
		InputStream is = null;
		Context context = null;
		int checking = 0;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response = httpclient.execute(httppost);

			int athul = response.getStatusLine().getStatusCode();
			Log.d("json response", Integer.toString(athul));
			// int athul = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			System.out.println("*******result********"+ result);
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		return result;

	}

	public static JSONObject name() {
		try 
		{
			jArray = new JSONObject(result);
		}

		catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());

		}
		return jArray;
	}
}
