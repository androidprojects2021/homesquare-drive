package com.homesquad.services;

import android.util.Log;

import com.homesquad.ServerUtilities;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by Leo elstin on 3/22/18.
 */

public class FirebaseTokenRegister extends FirebaseInstanceIdService {
    private static final String TAG = "response ";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("Op", "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
        FirebaseMessaging.getInstance().subscribeToTopic("driver");


    }

    private void sendRegistrationToServer(String refreshedToken) {


        ServerUtilities.register(this,refreshedToken);
    }
}
