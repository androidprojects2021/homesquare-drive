package com.homesquad.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.homesquad.driver.R;
import com.homesquad.model.Notification;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private List<Notification> mDataset;
    private Context context;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        TextView customer;
        TextView time;
        LinearLayout container;
        ImageView typeImage;

        MyViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.message);
            customer = v.findViewById(R.id.customer);
            time = v.findViewById(R.id.time);
            container = v.findViewById(R.id.container);
            typeImage = v.findViewById(R.id.typeImage);
        }
    }

    public NotificationAdapter(List<Notification> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_adapter, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        try {
            String[] string = mDataset.get(position).message.split(":");
            holder.mTextView.setText(String.format("Maid     : %s", string[1].replace(", Customer", "")));
            holder.customer.setText(String.format("Customer : %s", string[2].replace(", Shift", "")));

            String[] timeString = mDataset.get(position).message.split("Shift :");

//            holder.time.setText(String.format("Time : %s", string[3] + string[4] + " - " + string[5]));
            holder.time.setText(String.format("Time : %s", timeString[1]));

            if (mDataset.get(position).message.contains("New")) {
                holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.new_booking));
            } else if (mDataset.get(position).message.contains("Updated")) {
                holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.booking_change));
            } else if (mDataset.get(position).message.contains("Cancelled")) {
                holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.booking_cancel));
            } else if (mDataset.get(position).message.contains("Delete")) {
                holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.booking_cancel));
            } else if (mDataset.get(position).message.contains("Transfer")) {
                holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.transfer_maid));
            }

            if (position % 2 == 0) {

                holder.container.setBackgroundColor(Color.parseColor("#FFF5F5F5"));

            } else {
                holder.container.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}