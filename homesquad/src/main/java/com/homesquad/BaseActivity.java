package com.homesquad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;

import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.notification.Myservices;
import com.homesquad.notification.NotificationDioalogue;

public class BaseActivity extends FragmentActivity {

    public static boolean appInFront;
    MyReceiver myReceiver = new MyReceiver();


    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//		ServiceStart(); //commented  by abhi for below fix
        appInFront = true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        ServiceStart(); //added by abhi for fixingcrash and improvements
        appInFront = true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        appInFront = false;
        GlobalManager.getdata().setCurrentActivity("");
        unregisterReceiver(myReceiver); //added by abhi for fixing crash
    }

    public void ServiceStart() {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Myservices.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);

    }


    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {


//            Log.e(" fhsjhf0", arg1.getExtras().getString("isNotification"));

            if (arg1.getExtras().getString("isNotification") != null) {

                NotificationDioalogue.NotificationNew(arg0, arg1.getExtras().getString("heading"), arg1.getExtras().getString("message"), arg1.getExtras().getString("isNotification"));
                return;
            }

            // TODO Auto-generated method stub
            NotificationDioalogue.Notification(arg0, arg1.getExtras().getString("heading"), arg1.getExtras().getString("message"));
        }

    }


}
