package com.homesquad;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.database.customer.SynchronizeCustomer;
import com.homesquad.database.maid.ImageLoderutil;
import com.homesquad.database.maid.SynchronizeMaid;
import com.homesquad.driver.R;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;

public class Synchronize extends BaseActivity implements BaseInterface
{
	public Button buttonSynchronizemaid;
	public Button buttonSynchronizecustomer;
	Button buttonContinue;
	public Button buttonSynchronizecustomerTomorrow;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_synchronious);
		GlobalManager.getdata().setCurrentActivity("Synchronize");
//		Pushnotification.initPush(Synchronize.this);
		initUI();
		clickFunction();

	}

	@Override
	public void initUI()
	{
		buttonSynchronizemaid = (Button) findViewById(R.id.Synchronize_btn_maid);
		buttonSynchronizecustomer = (Button) findViewById(R.id.Synchronize_btn_customer);
		buttonContinue = (Button) findViewById(R.id.Synchronize_btn_continue);
		buttonSynchronizecustomerTomorrow = (Button) findViewById(R.id.Synchronize_btn_customertomorrow);
		ImageLoderutil.retriveMaid(Synchronize.this);

	}

	@Override
	public void clickFunction()
	{

		String _strExtra = getIntent().getExtras().getString("page");
		if (_strExtra.equalsIgnoreCase("1"))
		{
			CommonUtil.synchrinoouserror(Synchronize.this, "Update Error", "Please Update Again");
		}
		else if (_strExtra.equalsIgnoreCase("2"))
		{
			CommonUtil.synchrinoouserror(Synchronize.this, "Update Required", "Please Update Again");
		}
		

		if (URLUtil.ismadeSynchronize)
		{
			buttonSynchronizemaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidsync_tick));
		}
		else
		{
			buttonSynchronizemaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidsync_cross));
		}
		if (URLUtil.isTodaySynchronize)
		{
			if (!URLUtil.strSynchronizeDate.equalsIgnoreCase(URLUtil.getCurrentDate(Synchronize.this)))
			{
				buttonSynchronizecustomer.setBackgroundDrawable(getResources().getDrawable(R.drawable.today_cross));
			}
			else
			{
				buttonSynchronizecustomer.setBackgroundDrawable(getResources().getDrawable(R.drawable.today_tick));
			}
		}
		else
		{
			buttonSynchronizecustomer.setBackgroundDrawable(getResources().getDrawable(R.drawable.today_cross));
		}
		if (URLUtil.istomorrowSynchronize)
		{
			buttonSynchronizecustomerTomorrow.setBackgroundDrawable(getResources().getDrawable(R.drawable.tomorrow_tick));
		}
		else
		{
			buttonSynchronizecustomerTomorrow.setBackgroundDrawable(getResources().getDrawable(R.drawable.tomorrow_cross));
		}

		// if
		// (!URLUtil.strSynchronizeDateTomorrow.equalsIgnoreCase(URLUtil.getCurrentDate(Synchronize.this)))
		// {
		// buttonSynchronizecustomerTomorrow.setBackgroundDrawable(getResources().getDrawable(R.drawable.tomorrow_cross));
		// }
		// else
		// {
		// buttonSynchronizecustomerTomorrow.setBackgroundDrawable(getResources().getDrawable(R.drawable.tomorrow_tick));
		// }
		//
		// if (!ImageLoderutil.isSynchronize())
		// {
		// buttonSynchronizemaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidsync_cross));
		// }
		// else
		// {
		// buttonSynchronizemaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidsync_tick));
		// }

	
		buttonSynchronizemaid.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				CustomeDialogue();

			}
		});
		buttonSynchronizecustomer.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				SynchronizeCustomer.diologueSynchronize(Synchronize.this, "1");

			}
		});

		buttonSynchronizecustomerTomorrow.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				SynchronizeCustomer.diologueSynchronize(Synchronize.this, "2");

			}
		});
		buttonContinue.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				URLUtil.activateSharepreference(Synchronize.this);

				if (!SynchronizeCustomer.retriveCustomer(Synchronize.this)
						&& (!URLUtil.strSynchronizeDate.equalsIgnoreCase(URLUtil.getCurrentDate(Synchronize.this))))
				{
					CommonUtil.CustomeDialogue(Synchronize.this, " Synchronize Error", "Customer Details not Synchronize");
				}
				else if (!ImageLoderutil.isSynchronize())
				{
					CommonUtil.CustomeDialogue(Synchronize.this, " Synchronize Error", "Maid Details not Synchronize");
				}
				else if (!URLUtil.strSynchronizeDate.equalsIgnoreCase(URLUtil.getCurrentDate(Synchronize.this)))
				{
					CommonUtil.CustomeDialogue(Synchronize.this, " Synchronize Error", "Customer Details not Synchronize");
				}
				else
				{
					Intent oIntent = new Intent(Synchronize.this, HomepageActivity.class);
					oIntent.putExtra("page", "0");
					startActivity(oIntent);
					finish();
					overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
				}
			}
		});
	}
	
	
	public  void CustomeDialogue()
	{

		

		final Dialog dialog1 = new Dialog(Synchronize.this);

		dialog1.setContentView(R.layout.customdialogue_password);

		dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog1.setCancelable(true);
		TextView textViewHeading = (TextView) dialog1.findViewById(R.id.customeerror_heading);
		final EditText textViewContent = (EditText) dialog1.findViewById(R.id.customeerror_message);
		Button buttonOk = (Button) dialog1.findViewById(R.id.customeerror_btn);
	
		

		buttonOk.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				if(textViewContent.getText().toString().trim().equals("12345"))
				{
					SynchronizeMaid.diologueSynchronize(Synchronize.this, 0);
					dialog1.dismiss();
				}
				
				
			}
		});

		dialog1.show();

	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
//		Pushnotification.ondestory(Synchronize.this);
		super.onDestroy();
	}
}
