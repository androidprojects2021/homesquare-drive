package com.homesquad.database.maid;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.homesquad.driver.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class ImageLoderutil {

	public static DisplayImageOptions options;
	public static ImageLoader imageLoader;

	private static MySQLiteHelper oMySQLiteHelper;

	private static List<FetchData> data = new ArrayList<FetchData>();

	public static void retriveMaid(Activity a)
	{
		oMySQLiteHelper = new MySQLiteHelper(a);
		oMySQLiteHelper.getWritableDatabase();
		data.clear();
		data = oMySQLiteHelper.returnvalue();
	}

	public static boolean isSynchronize()
	{

		if (data.size() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public static void imageBinding(final Activity activity, String id, final ImageView oImageView, final ProgressBar oProgressBar)
	{

		
		Boolean boolMaidPresent = false;
		oProgressBar.setVisibility(View.VISIBLE);

		for (int i = 0; i < data.size(); i++)
		{
			if (id.equalsIgnoreCase(data.get(i).get_maidname()))
			{

				boolMaidPresent = true;

				byte[] img1 = data.get(i).get_maidphoto();

				Bitmap oBitmap = BitmapFactory.decodeByteArray(img1, 0, img1.length);

				oImageView.setImageBitmap(oBitmap);

				break;
			}

		}
		if (boolMaidPresent == false)
		{
			oImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_empty));
		}
		oProgressBar.setVisibility(View.GONE);
	}

	

	

	

}
