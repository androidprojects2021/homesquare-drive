package com.homesquad.database.maid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TypedValue;

import com.homesquad.Synchronize;
import com.homesquad.driver.R;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class SynchronizeMaid {

    private static Activity _activity;
    private static ProgressDialog _objProgressDialog;
    private static int count = 0;
    private static JSONArray oJsonArray;
    private static MySQLiteHelper oMySQLiteHelper;
    private static List<String> maidid = new ArrayList<String>();
    private static int _id = 0;

    public static void diologueSynchronize(Activity a, int id)// if id = 1 from
    // home page
    {

        _activity = a;
        _id = id;
        _objProgressDialog = new ProgressDialog(a);
        _objProgressDialog.setMessage("Synchronise maid....");
        _objProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        _objProgressDialog.setIndeterminate(false);
        _objProgressDialog.setProgress(0);
        _objProgressDialog.setCancelable(false);
        _objProgressDialog.show();

        oMySQLiteHelper = new MySQLiteHelper(_activity);
        oMySQLiteHelper.getWritableDatabase();

        count = 0;
        AsktaskDemo(URLUtil.maidSynchronizeURL());

    }

    private static void AsktaskDemo(String URL)

    {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.get(URL, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String arg0) {
                // TODO Auto-generated method stub
                super.onSuccess(arg0);

                try {
                    JSONObject oJsonObject = new JSONObject(arg0);
                    if (oJsonObject.getString("status").equalsIgnoreCase("success")) {
                        oMySQLiteHelper.deleteTable();
                        maidid.clear();

                        oJsonArray = oJsonObject.getJSONArray("maids");
                        _objProgressDialog.setMax(oJsonArray.length());
                        for (int i = 0; i < oJsonArray.length(); i++) {
                            JSONObject ojsonObject1 = oJsonArray.getJSONObject(i);
                            maidid.add(ojsonObject1.getString("id"));

                            new GetXMLTask().execute(ojsonObject1.getString("photo"));

                        }
                        URLUtil.edit.putBoolean("madesynchronize", true);
                        URLUtil.edit.commit();
                        ((Synchronize) _activity).buttonSynchronizemaid.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.maidsync_tick));

                    } else {
                        URLUtil.edit.putBoolean("madesynchronize", false);
                        URLUtil.edit.commit();
                        ((Synchronize) _activity).buttonSynchronizemaid.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.maidsync_cross));
                        CommonUtil.CustomeDialogue(_activity, "Synchronize Fail", "Maid Details Not Synchronize");
                        _objProgressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    URLUtil.edit.putBoolean("madesynchronize", false);
                    URLUtil.edit.commit();
                    ((Synchronize) _activity).buttonSynchronizemaid.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.maidsync_cross));
                    CommonUtil.CustomeDialogue(_activity, "Synchronize Fail", "Maid Details Not Synchronize");
                    _objProgressDialog.dismiss();

                }

            }

            @Override
            public void onFailure(Throwable arg0) {
                // TODO Auto-generated method stub
                super.onFailure(arg0);
                URLUtil.edit.putBoolean("madesynchronize", false);
                URLUtil.edit.commit();
                ((Synchronize) _activity).buttonSynchronizemaid.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.maidsync_cross));
                CommonUtil.CustomeDialogue(_activity, "Synchronize Fail", "Maid Details Not Synchronize");
                _objProgressDialog.dismiss();

            }

        });

    }

    private static class GetXMLTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap map = null;
            for (String url : urls) {


                if (downloadImage(url) != null) {
                    map = downloadImage(url);
                }

            }
            return map;

        }

        @Override
        protected void onPostExecute(Bitmap result) {

            count = count + 1;
            _objProgressDialog.setProgress(count);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            if (result == null) {

                Bitmap largeIcon = BitmapFactory.decodeResource(_activity.getResources(), R.drawable.nophoto);
                largeIcon.compress(Bitmap.CompressFormat.PNG, 100, bos);
            } else {
                result.compress(Bitmap.CompressFormat.PNG, 100, bos);
            }

            byte[] img = bos.toByteArray();
            String id = maidid.get(count - 1);
            oMySQLiteHelper.adddata(id, img);
            if (count == oJsonArray.length()) {

                ImageLoderutil.retriveMaid(_activity);
                _objProgressDialog.dismiss();

            }

        }
    }

    // Creates Bitmap from InputStream and returns it
    private static Bitmap downloadImage(String url) {
        Bitmap bitmap = null;
        InputStream stream = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 1;

        try {
            stream = getHttpConnection(url);
            bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
            bitmap = getRoundedShape(bitmap, 140, 140, _activity);
            stream.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage, int targetWidth, int targetHeight, Context context) {
        // TODO Auto-generated method stub
        // int targetWidth = width;
        // int targetHeight = height;

        targetHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, targetHeight, (context).getResources().getDisplayMetrics());
        targetWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, targetWidth, (context).getResources().getDisplayMetrics());

        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth) / 2, ((float) targetHeight) / 2, (Math.min(((float) targetWidth), ((float) targetHeight)) / 2), Path.Direction.CCW);
        Log.d("", "In process: width" + targetWidth + " height" + targetHeight);
        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight()), new Rect(0, 0, targetWidth, targetHeight), null);
        return targetBitmap;
    }

    // Makes HttpURLConnection and returns InputStream
    private static InputStream getHttpConnection(String urlString) throws IOException {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        try {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();

            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                stream = httpConnection.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stream;
    }

}
