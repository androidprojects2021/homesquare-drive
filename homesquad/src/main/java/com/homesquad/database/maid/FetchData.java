package com.homesquad.database.maid;


public class FetchData
{
	String _maidname;
	byte[] _maidphoto;

	public FetchData()
	{

	}

	public String get_maidname()
	{
		return _maidname;
	}

	public void set_maidname(String _maidname)
	{
		this._maidname = _maidname;
	}

	public byte[] get_maidphoto()
	{
		return _maidphoto;
	}

	public void set_maidphoto(byte[] _maidphoto)
	{
		this._maidphoto = _maidphoto;
	}

}
