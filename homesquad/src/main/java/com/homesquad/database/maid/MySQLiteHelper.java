package com.homesquad.database.maid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.emaid.azinova.EmaidCustomerDetailsURLContainer;

import java.util.ArrayList;
import java.util.List;

import model.EmaidCustomerDetailsURLContainerNew;

public class MySQLiteHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "main_logo.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLENAME = "maid";
	private static final String COLUMN_ID = "id";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_IMAGE = "image";

	private static String TEXT = " TEXT";
	private static String COMA = ", ";

	private static String TABENAME2 = "customer";
	private static String TABENAME3 = "customertomorrow";
	private static final String COLUMNPRIMARY_ID = "id";
	private static final String TABLE2BOOKINGID = "bookingid";
	private static final String TABLE2SERVICEFEE = "service_fee";
	private static final String TABLE2CLEANING = "cleaning_material";
	private static final String TABLE2LATITUDE = "lattitude";
	private static final String TABLE2LONGITUDE = "longitude";
	private static final String TABLE2CUSTOMERID = "customerid";
	private static final String TABLE2CUSTOMERCODE = "customercode";
	private static final String TABLE2CUSTOMERADDRESS = "customeraddress";
	private static final String TABLE2CUSTOMERNAME = "customername";
	private static final String TABLE2CUSTOMERMOBILE = "customermobile";
	private static final String TABLE2CUSTOMERTYPE = "customertype";
	private static final String TABLE2CUSTOMERSTATUS = "customerstatus";
	private static final String TABLE2MAIDID = "maidid";
	private static final String TABLE2MAIDNAME = "maidname";
	private static final String TABLE2MAIDCOUNTRY = "maidcountry";
	private static final String TABLE2MAIDSTATUS = "maidstatus";
	private static final String TABLE2SHIFTSTART = "shiftstart";
	private static final String TABLE2SHIFTEND = "shiftstop";
	private static final String TABLE2KEYSTATUS = "keystatus";
	private static final String TABLE2AREA = "area";
	private static final String TABLE2APPARTMENTNUMBER = "apartmentnumber";
	private static final String TABLE2BOOKINGNOTES = "bookingnotes";
	private static final String TABLE2SERVIESSTATUS = "servicesstatus";
	private static final String TABLE2CUSTOMERLAN = "lan";
	private static final String TABLE2CUSTOMERLON = "lon";

	String createquery = "CREATE TABLE " + TABLENAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY" + ", " + COLUMN_NAME + " TEXT" + ", " + COLUMN_IMAGE + " BLOB" + ");";
	String createquery1 = "CREATE TABLE " + TABENAME2 + "(" + COLUMNPRIMARY_ID + " INTERGER PRIMARY KEY" + COMA + TABLE2CUSTOMERID + TEXT + COMA + TABLE2CUSTOMERCODE + TEXT + COMA
			+ TABLE2CUSTOMERNAME + TEXT + COMA + TABLE2CUSTOMERADDRESS + TEXT + COMA + TABLE2CUSTOMERMOBILE + TEXT + COMA + TABLE2CUSTOMERTYPE + TEXT + COMA + TABLE2CUSTOMERSTATUS + TEXT + COMA
			+ TABLE2MAIDID + TEXT + COMA + TABLE2MAIDNAME + TEXT + COMA + TABLE2MAIDCOUNTRY + TEXT + COMA + TABLE2MAIDSTATUS + TEXT + COMA + TABLE2SHIFTSTART + TEXT + COMA + TABLE2SHIFTEND + TEXT
			+ COMA + TABLE2KEYSTATUS + TEXT + COMA + TABLE2AREA + TEXT + COMA + TABLE2BOOKINGNOTES + TEXT + COMA + TABLE2BOOKINGID + TEXT + COMA + TABLE2SERVICEFEE + TEXT + COMA + TABLE2CLEANING + TEXT + COMA + TABLE2SERVIESSTATUS + TEXT + TABLE2CUSTOMERLAN
			+ TEXT + COMA + TABLE2CUSTOMERLON + TEXT + COMA + TABLE2LONGITUDE + TEXT + COMA + TABLE2LATITUDE + TEXT + COMA + TABLE2APPARTMENTNUMBER +");";

	String createquery2 = "CREATE TABLE " + TABENAME3 + "(" + COLUMNPRIMARY_ID + " INTERGER PRIMARY KEY" + COMA + TABLE2CUSTOMERID + TEXT + COMA + TABLE2CUSTOMERCODE + TEXT + COMA
			+ TABLE2CUSTOMERNAME + TEXT + COMA + TABLE2CUSTOMERADDRESS + TEXT + COMA + TABLE2CUSTOMERMOBILE + TEXT + COMA + TABLE2CUSTOMERTYPE + TEXT + COMA + TABLE2CUSTOMERSTATUS + TEXT + COMA
			+ TABLE2MAIDID + TEXT + COMA + TABLE2MAIDNAME + TEXT + COMA + TABLE2MAIDCOUNTRY + TEXT + COMA + TABLE2MAIDSTATUS + TEXT + COMA + TABLE2SHIFTSTART + TEXT + COMA + TABLE2SHIFTEND + TEXT
			+ COMA + TABLE2KEYSTATUS + TEXT + COMA + TABLE2AREA + TEXT + COMA + TABLE2BOOKINGNOTES + TEXT + COMA + TABLE2BOOKINGID + TEXT + COMA + TABLE2SERVICEFEE + TEXT + COMA + TABLE2CLEANING + TEXT + COMA + TABLE2SERVIESSTATUS + TEXT + TABLE2CUSTOMERLAN
			+ TEXT + COMA + TABLE2CUSTOMERLON + TEXT + COMA + TABLE2LONGITUDE + TEXT + COMA + TABLE2LATITUDE + TEXT + COMA + TABLE2APPARTMENTNUMBER  + ");";

	public MySQLiteHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		// TODO Auto-generated method stub
		System.out.println("******" + createquery1);
		db.execSQL(createquery);
		db.execSQL(createquery1);
		db.execSQL(createquery2);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{

	}

	public void deleteTable()
	{
		SQLiteDatabase database = this.getWritableDatabase();
		database.execSQL("delete from " + TABLENAME);
		database.close();
	}

	public void adddata(String name, byte[] img)
	{
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(COLUMN_NAME, name);
		values.put(COLUMN_IMAGE, img);
		database.insert(TABLENAME, null, values);
		database.close();

	}

	public void addcustomerData(String cusid, String cuscode, String cusname, String cusaddress, String cusmoblie, String custype, String cusstatus, String maidid, String maidname,
			String maidcountry, String maidstatus, String shiftstart, String shiftend, String keystatus, String area,String apartmentNumber, String bookingnotes, String servicesstatus, String booking_id,
			String service_fee,String cleaning_material, String latitude, String longitude)
	{
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(TABLE2CUSTOMERID, cusid);
		values.put(TABLE2CUSTOMERCODE, cuscode);
		values.put(TABLE2CUSTOMERNAME, cusname);
		values.put(TABLE2CUSTOMERADDRESS, cusaddress);
		values.put(TABLE2CUSTOMERMOBILE, cusmoblie);
		values.put(TABLE2CUSTOMERTYPE, custype);
		values.put(TABLE2CUSTOMERSTATUS, cusstatus);
		values.put(TABLE2MAIDID, maidid);
		values.put(TABLE2MAIDNAME, maidname);
		values.put(TABLE2MAIDCOUNTRY, maidcountry);
		values.put(TABLE2MAIDSTATUS, maidstatus);
		values.put(TABLE2SHIFTSTART, shiftstart);
		values.put(TABLE2SHIFTEND, shiftend);
		values.put(TABLE2KEYSTATUS, keystatus);
		values.put(TABLE2AREA, area);
		values.put(TABLE2APPARTMENTNUMBER, apartmentNumber);
		values.put(TABLE2APPARTMENTNUMBER, area);
		values.put(TABLE2BOOKINGNOTES, bookingnotes);
		values.put(TABLE2SERVIESSTATUS, servicesstatus);
		values.put(TABLE2BOOKINGID, booking_id);
		values.put(TABLE2SERVICEFEE, service_fee);
		values.put(TABLE2CLEANING, cleaning_material);
		values.put(TABLE2LATITUDE, latitude);
		values.put(TABLE2LONGITUDE, longitude);
		
		database.insert(TABENAME2, null, values);

		database.close();
	}

	public void addcustomerDataTomorrow(String cusid, String cuscode, String cusname, String cusaddress, String cusmoblie, String custype, String cusstatus, String maidid, String maidname,
			String maidcountry, String maidstatus, String shiftstart, String shiftend, String keystatus, String area,String apartmentNumber, String bookingnotes, String servicesstatus, String booking_id,
			String service_fee,String cleaning_material, String latitude, String longitude)
	{
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(TABLE2CUSTOMERID, cusid);
		values.put(TABLE2CUSTOMERCODE, cuscode);
		values.put(TABLE2CUSTOMERNAME, cusname);
		values.put(TABLE2CUSTOMERADDRESS, cusaddress);
		values.put(TABLE2CUSTOMERMOBILE, cusmoblie);
		values.put(TABLE2CUSTOMERTYPE, custype);
		values.put(TABLE2CUSTOMERSTATUS, cusstatus);
		values.put(TABLE2MAIDID, maidid);
		values.put(TABLE2MAIDNAME, maidname);
		values.put(TABLE2MAIDCOUNTRY, maidcountry);
		values.put(TABLE2MAIDSTATUS, maidstatus);
		values.put(TABLE2SHIFTSTART, shiftstart);
		values.put(TABLE2SHIFTEND, shiftend);
		values.put(TABLE2KEYSTATUS, keystatus);
		values.put(TABLE2AREA, area);
		values.put(TABLE2APPARTMENTNUMBER, apartmentNumber);
		values.put(TABLE2BOOKINGNOTES, bookingnotes);
		values.put(TABLE2SERVIESSTATUS, servicesstatus);
		values.put(TABLE2BOOKINGID, booking_id);
		values.put(TABLE2SERVICEFEE, service_fee);
		values.put(TABLE2CLEANING, cleaning_material);
        values.put(TABLE2LATITUDE, latitude);
        values.put(TABLE2LONGITUDE, longitude);
		
		database.insert(TABENAME3, null, values);

		database.close();
	}

	public void deleteTableCustomer()
	{
		SQLiteDatabase database = this.getWritableDatabase();
		database.execSQL("delete from " + TABENAME2);
		database.close();
	}

	public void deleteTableCustomerTomorrow()
	{
		SQLiteDatabase database = this.getWritableDatabase();
		database.execSQL("delete from " + TABENAME3);
		database.close();
	}

	public List<FetchData> returnvalue()
	{
		List<FetchData> maidList = new ArrayList<FetchData>();
		byte[] img = null;
		SQLiteDatabase sqliteDatabase = this.getReadableDatabase();
		Cursor cursor = sqliteDatabase.query(this.TABLENAME, null, null, null, null, null, null);

		// Cursor object read all the fields. So we make sure to check it will
		// not miss any by looping through a while loop
		while (cursor.moveToNext())
		{
			FetchData contact = new FetchData();
			String ugName = cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_NAME));
			img = cursor.getBlob(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE));
			contact.set_maidname(ugName);
			contact.set_maidphoto(img);

			maidList.add(contact);
		}
		sqliteDatabase.close();
		return maidList;
	}

	// ************FOR GETTING A CUSTOMER SORT BY BOOKINGID
	// (TODAY)*************************
	public List<EmaidCustomerDetailsURLContainerNew> returnSinglecustomer(String bookingid)
	{

		List<EmaidCustomerDetailsURLContainerNew> maidList = new ArrayList<EmaidCustomerDetailsURLContainerNew>();

		SQLiteDatabase sqliteDatabase = this.getReadableDatabase();

		Cursor cursor = sqliteDatabase.rawQuery("SELECT * FROM " + TABENAME2 + " where " + TABLE2BOOKINGID + " = " + "\"" + bookingid.trim() + "\"", null);

		// Cursor object read all the fields. So we make sure to check it will
		// not miss any by looping through a while 
		while (cursor.moveToNext())
		{
			EmaidCustomerDetailsURLContainerNew contact = new EmaidCustomerDetailsURLContainerNew();

			contact.setCustomer_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERID)));
			contact.setCustomer_code(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERCODE)));
			contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERNAME)));
			contact.setCustomer_address(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERADDRESS)));
			contact.setCustomer_mobile(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERMOBILE)));
			contact.setCustomer_type(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERTYPE)));
			contact.setCustomer_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERSTATUS)));
			contact.setMaid_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDID)));
			contact.setMaid_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDNAME)));
			contact.setMaid_country(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDCOUNTRY)));
			contact.setMaid_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDSTATUS)));
			contact.setShift_start(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTSTART)));
			contact.setShift_end(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTEND)));
			contact.setKey_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2KEYSTATUS)));
			contact.setArea(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2AREA)));
			contact.setApartment_number(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2APPARTMENTNUMBER)));
			contact.setBooking_note(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGNOTES)));
			contact.setService_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVIESSTATUS)));
			contact.setBooking_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGID)));
			contact.setService_fee(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVICEFEE)));
			contact.setCleaning_material(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CLEANING)));
			contact.setCustomer_longitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LONGITUDE)));
            contact.setCustomer_latitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LATITUDE)));

			maidList.add(contact);
		}
		sqliteDatabase.close();
		return maidList;
	}

	// ************FOR GETTING A CUSTOMER SORT BY BOOKINGID
	// (TOMORROW)*************************
	public List<EmaidCustomerDetailsURLContainerNew> returnSinglecustomerTomorrow(String bookingid)
	{

		List<EmaidCustomerDetailsURLContainerNew> maidList = new ArrayList<EmaidCustomerDetailsURLContainerNew>();

		SQLiteDatabase sqliteDatabase = this.getReadableDatabase();

		Cursor cursor = sqliteDatabase.rawQuery("SELECT * FROM " + TABENAME3 + " where " + TABLE2BOOKINGID + " = " + "\"" + bookingid.trim() + "\"", null);

		// Cursor object read all the fields. So we make sure to check it will
		// not miss any by looping through a while loop
		while (cursor.moveToNext())
		{
			EmaidCustomerDetailsURLContainerNew contact = new EmaidCustomerDetailsURLContainerNew();

			contact.setCustomer_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERID)));
			contact.setCustomer_code(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERCODE)));
			contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERNAME)));
			contact.setCustomer_address(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERADDRESS)));
			contact.setCustomer_mobile(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERMOBILE)));
			contact.setCustomer_type(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERTYPE)));
			contact.setCustomer_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERSTATUS)));
			contact.setMaid_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDID)));
			contact.setMaid_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDNAME)));
			contact.setMaid_country(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDCOUNTRY)));
			contact.setMaid_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDSTATUS)));
			contact.setShift_start(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTSTART)));
			contact.setShift_end(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTEND)));
			contact.setKey_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2KEYSTATUS)));
			contact.setArea(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2AREA)));
			contact.setApartment_number(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2APPARTMENTNUMBER)));
			contact.setBooking_note(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGNOTES)));
			contact.setService_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVIESSTATUS)));
			contact.setBooking_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGID)));
			contact.setService_fee(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVICEFEE)));
			contact.setCleaning_material(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CLEANING)));

            contact.setCustomer_longitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LONGITUDE)));
            contact.setCustomer_latitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LATITUDE)));

			maidList.add(contact);
		}
		sqliteDatabase.close();
		return maidList;
	}

	// ************FOR GETTING ALL LIST OF CUSTOMER SORT BY START TIME
	// (TODAY)*************************
	public List<EmaidCustomerDetailsURLContainerNew> returncustomer(String starttime, String string)
	{

		List<EmaidCustomerDetailsURLContainerNew> maidList = new ArrayList<EmaidCustomerDetailsURLContainerNew>();

		SQLiteDatabase sqliteDatabase = this.getReadableDatabase();

		Cursor cursor = sqliteDatabase.rawQuery("SELECT * FROM " + TABENAME2 + " where " + TABLE2SHIFTSTART + " = " + "\"" + starttime.trim() +"\"" +" or "+ TABLE2SHIFTSTART + " = " + "\"" + string.trim()+ "\"" + " ORDER BY servicesstatus ASC", null);

		// Cursor object read all the fields. So we make sure to check it will
		// not miss any by looping through a while loop
		while (cursor.moveToNext())
		{

			EmaidCustomerDetailsURLContainerNew contact = new EmaidCustomerDetailsURLContainerNew();

			contact.setCustomer_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERID)));
			contact.setCustomer_code(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERCODE)));
			contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERNAME)));
			contact.setCustomer_address(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERADDRESS)));
			contact.setCustomer_mobile(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERMOBILE)));
			contact.setCustomer_type(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERTYPE)));
			contact.setCustomer_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERSTATUS)));
			contact.setMaid_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDID)));
			contact.setMaid_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDNAME)));
			contact.setMaid_country(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDCOUNTRY)));
			contact.setMaid_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDSTATUS)));
			contact.setShift_start(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTSTART)));
			contact.setShift_end(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTEND)));
			contact.setKey_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2KEYSTATUS)));
			contact.setArea(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2AREA)));
			contact.setApartment_number(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2APPARTMENTNUMBER)));
			contact.setBooking_note(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGNOTES)));
			contact.setService_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVIESSTATUS)));
			contact.setBooking_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGID)));
			contact.setService_fee(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVICEFEE)));
			contact.setCleaning_material(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CLEANING)));

            contact.setCustomer_longitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LONGITUDE)));
            contact.setCustomer_latitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LATITUDE)));
			
			maidList.add(contact);

		}
		// System.out.println("*********"+maidList.get(0));

		return maidList;
	}

	// ************FOR GETTING ALL LIST OF CUSTOMER SORT BY START TIME
	// (TOMORROW)*************************
	public List<EmaidCustomerDetailsURLContainerNew> returncustomerTomorrow(String starttime, String string)
	{

		List<EmaidCustomerDetailsURLContainerNew> maidList = new ArrayList<EmaidCustomerDetailsURLContainerNew>();

		SQLiteDatabase sqliteDatabase = this.getReadableDatabase();

		Cursor cursor = sqliteDatabase.rawQuery("SELECT * FROM " + TABENAME3 + " where " + TABLE2SHIFTSTART + " = " + "\"" + starttime.trim() +"\"" + " or "+ TABLE2SHIFTSTART + " = " + "\"" + string.trim() +"\"" + " ORDER BY servicesstatus ASC", null);

		while (cursor.moveToNext())
		{

			EmaidCustomerDetailsURLContainerNew contact = new EmaidCustomerDetailsURLContainerNew();

			contact.setCustomer_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERID)));
			contact.setCustomer_code(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERCODE)));
			contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERNAME)));
			contact.setCustomer_address(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERADDRESS)));
			contact.setCustomer_mobile(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERMOBILE)));
			contact.setCustomer_type(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERTYPE)));
			contact.setCustomer_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERSTATUS)));
			contact.setMaid_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDID)));
			contact.setMaid_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDNAME)));
			contact.setMaid_country(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDCOUNTRY)));
			contact.setMaid_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2MAIDSTATUS)));
			contact.setShift_start(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTSTART)));
			contact.setShift_end(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SHIFTEND)));
			contact.setKey_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2KEYSTATUS)));
			contact.setArea(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2AREA)));
			contact.setApartment_number(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2APPARTMENTNUMBER)));
			contact.setBooking_note(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGNOTES)));
			contact.setService_status(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVIESSTATUS)));
			contact.setBooking_id(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2BOOKINGID)));
			contact.setService_fee(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2SERVICEFEE)));
			contact.setCleaning_material(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CLEANING)));

            contact.setCustomer_longitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LONGITUDE)));
            contact.setCustomer_latitude(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2LATITUDE)));
			
			maidList.add(contact);

		}

		return maidList;
	}

	// ************FOR MAID ATTENDANCE*************************
	public void setMaidAttendance(String id, String value)
	{
		SQLiteDatabase sqliteDatabase = this.getWritableDatabase();
		String query = "UPDATE " + TABENAME2 + " SET " + TABLE2MAIDSTATUS + " = " + value + " WHERE " + TABLE2MAIDID + " = " + id;
try
{
	

		sqliteDatabase.execSQL(query);
}
catch (Exception e)
{
	// TODO: handle exception
}
	}

	// ************FOR CUSTOMER ATTENDANCE*************************
	public void setCustomerAttendance(String id, String value)
	{
		SQLiteDatabase sqliteDatabase = this.getWritableDatabase();
		String query = "UPDATE " + TABENAME2 + " SET " + TABLE2CUSTOMERSTATUS + " = " + value + " WHERE " + TABLE2BOOKINGID + " = " + id;

		sqliteDatabase.execSQL(query);

		String query2 = "UPDATE " + TABENAME2 + " SET " + TABLE2SERVIESSTATUS + " = " + value + " WHERE " + TABLE2BOOKINGID + " = " + id;
		sqliteDatabase.execSQL(query2);
	}

	// *******************FOR TRANSFERMAID************
	public void setTransfermaid(String maidid)
	{

		SQLiteDatabase sqliteDatabase = this.getWritableDatabase();
		String query = "DELETE FROM " + TABENAME2 + " WHERE " + TABLE2MAIDID + " = " + maidid + " AND " + TABLE2SERVIESSTATUS + " IN (0,1)";

		sqliteDatabase.execSQL(query);

	}

	// *******************FOR CHECKING ANY VALUES IN THE TABLE************
	public List<EmaidCustomerDetailsURLContainer> returncustomer()
	{

		List<EmaidCustomerDetailsURLContainer> maidList = new ArrayList<EmaidCustomerDetailsURLContainer>();

		SQLiteDatabase sqliteDatabase = this.getReadableDatabase();

		Cursor cursor = sqliteDatabase.rawQuery("SELECT * FROM " + TABENAME2, null);

		// Cursor object read all the fields. So we make sure to check it will
		// not miss any by looping through a while loop
		while (cursor.moveToNext())
		{
			EmaidCustomerDetailsURLContainer contact = new EmaidCustomerDetailsURLContainer();

			contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.TABLE2CUSTOMERNAME)));

			maidList.add(contact);

		}
		return maidList;
	}

}
