package com.homesquad.location;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.homesquad.utils.URLUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private static final String TAG = "Location Service";
    private GoogleApiClient mGoogleApiClient;

    public LocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationRequest mLocationRequest = new LocationRequest();

        /*mLocationRequest.setInterval(6000);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setSmallestDisplacement(100);*/

        mLocationRequest.setInterval(50000);
        mLocationRequest.setFastestInterval(50000);
        mLocationRequest.setSmallestDisplacement(200);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {


        updateLocation(location.getLatitude(), location.getLongitude(), location.getSpeed());


    }

    public class LocalBinder extends Binder {
        public LocationService getServerInstance() {
            return LocationService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.e(TAG, "onStartCommand: ");
        return START_STICKY;
    }

    @Override
    public void onCreate() {

        Log.e(TAG, "onCreate: ");
        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }
    }

    @Override
    public void onDestroy() {
        mGoogleApiClient.disconnect();
        Log.e(TAG, "onDestroy: ");
        super.onDestroy();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void updateLocation(Double lat, Double lng, float speed) {

        Log.e(" Lat lang ", lat.toString() + " " + lng.toString());

        String imei = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
        locationUpdate(URLUtil.getLocationUpdateURl(imei, String.valueOf(lat), String.valueOf(lng), String.valueOf(speed)));

    }

    public void locationUpdate(String URL) {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.setTimeout(30000);
        asyncHttpClient.get(URL, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onFailure(arg0, arg1);

            }

            @Override
            public void onSuccess(String arg0) {
                // TODO Auto-generated method stub
                super.onSuccess(arg0);

            }

        });
    }
}
