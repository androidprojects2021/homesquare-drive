//package com.emaid.location;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.location.Location;
//import android.os.Bundle;
//import android.provider.Settings.Secure;
//
//import URLUtil;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.LocationListener;
//import com.google.android.gms.location.LocationRequest;
//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.AsyncHttpResponseHandler;
//
//public class Locationservices extends BroadcastReceiver
//        implements GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener,
//        LocationListener {
//    LocationRequest mLocationRequest;
//    public static final int MILLISECONDS_PER_SECOND = 1000;
//
//    // The update interval
//    public static final int UPDATE_INTERVAL_IN_SECONDS = 60;
//
//    // A fast interval ceiling
//    public static final int FAST_CEILING_IN_SECONDS = 60;
//
//    // Update interval in milliseconds
//    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
//
//    // A fast ceiling of update intervals, used when the app is visible
//    public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * FAST_CEILING_IN_SECONDS;
//    GoogleApiClient mLocationClient;
//    Context context;
//
//    @Override
//    public void onReceive(Context context1, Intent intent) {
//        context = context1;
//        mLocationRequest = LocationRequest.create();
//
//        /*
//         * Set the update interval
//         */
//        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
//
//        // Use high accuracy
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//
//        // Set the interval ceiling to one minute
//        mLocationRequest.setFastestInterval(FAST_INTERVAL_CEILING_IN_MILLISECONDS);
//
//        mLocationClient = new GoogleApiClient.Builder(this)
//        if (mLocationClient.isConnected()) {
//            mLocationClient.disconnect();
//        }
//        mLocationClient.connect();
//
//    }
//
//    @Override
//    public void onLocationChanged(Location arg0) {
//
//
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult arg0) {
//
//
//    }
//
//    @Override
//    public void onConnected(Bundle arg0) {
//        mLocationClient.requestLocationUpdates(mLocationRequest, this);
//        if (servicesConnected(context)) {
//            Location currentLocation = mLocationClient
//            String imei = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
//            if (currentLocation != null) {
//                locationUpdate(URLUtil.getLocationUpdateURl(imei, String.valueOf(currentLocation.getLatitude()), String.valueOf(currentLocation.getLongitude()), String.valueOf(currentLocation.getSpeed())));
//            }
//
//
//        } else {
//
//        }
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//
//    @Override
//    public void onDisconnected() {
//        // TODO Auto-generated method stub
//
//    }
//
//
//    public static boolean servicesConnected(Context context) {
//
//        // Check that Google Play services is available
//        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
//
//        // If Google Play services is available
//        if (ConnectionResult.SUCCESS == resultCode) {
//
//            return true;
//
//        } else {
//
//            return false;
//        }
//    }
//
//    public void locationUpdate(String URL) {
//        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
//        asyncHttpClient.setTimeout(30000);
//        asyncHttpClient.get(URL, new AsyncHttpResponseHandler() {
//
//            @Override
//            public void onFailure(Throwable arg0, String arg1) {
//                // TODO Auto-generated method stub
//                super.onFailure(arg0, arg1);
//
//            }
//
//            @Override
//            public void onSuccess(String arg0) {
//                // TODO Auto-generated method stub
//                super.onSuccess(arg0);
//
//            }
//
//        });
//    }
//
//}
