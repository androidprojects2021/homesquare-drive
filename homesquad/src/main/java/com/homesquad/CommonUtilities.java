package com.homesquad;

import android.content.Context;
import android.content.Intent;

import com.homesquad.utils.URLUtil;

public final class CommonUtilities {

	// give your server registration url here
	
//	public static final String SERVER_URL = "http://demo.azinova.info/php/mymaid/api/registertablet?imei=" + URLUtil.strPhoneIMEI + "&regid=";//my maids demo url
	
//	public static final String SERVER_URL = "http://demo.azinova.info/php/mymaid-demo/api/registertablet?imei=" + URLUtil.strPhoneIMEI + "&regid=";//my maids demo url
	
	public static final String SERVER_URL = URLUtil.BASEURL+ "registertablet?imei=" + URLUtil.strPhoneIMEI + "&regid=";//Daily Help demo url
	public static final String SERVER_SECURITY = "";
	// Google project id
//	public static final String SENDER_ID = "994437144603";
	public static final String SENDER_ID = "804637671599";
	/**
	 * Tag used on log messages.
	 */
	public static final String TAG = "EMaid";

	public static final String DISPLAY_MESSAGE_ACTION = "com.azinova.EMaid.DISPLAY_MESSAGE";

	public static final String EXTRA_MESSAGE = "message";

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void displayMessage(Context context, String message)
	{
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
