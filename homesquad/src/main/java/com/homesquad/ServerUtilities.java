package com.homesquad;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.homesquad.driver.R;
import com.homesquad.services.JSONClass;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;

//import com.google.android.gcm.GCMRegistrar;

public final class ServerUtilities {
	private static final int MAX_ATTEMPTS = 5;
	private static final int BACKOFF_MILLI_SECONDS = 2000;
	private static final Random random = new Random();

	/**
	 * Register this account/device pair within the server.
	 * 
	 */
	// static void register(final Context context, String name, String email,
	// final String regId) {
	public static void register(final Context context, final String regId)
	{
		Log.i(CommonUtilities.TAG, "registering device (regId = " + regId + ")");
		String serverUrl = CommonUtilities.SERVER_URL + regId;

		System.out.println("***ccccc******" + regId);
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// Once GCM returns a registration id, we need to register on our server
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++)
		{
			Log.d(CommonUtilities.TAG, "Attempt #" + i + " to register");
			try
			{
				CommonUtilities.displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
				post(context, serverUrl, regId);
//				GCMRegistrar.setRegisteredOnServer(context, true);
				String message = context.getString(R.string.server_registered);
				CommonUtilities.displayMessage(context, message);

				return;
			}
			catch (Exception e)
			{
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				Log.e(CommonUtilities.TAG, "Failed to register on attempt " + i + ":" + e);
				if (i == MAX_ATTEMPTS)
				{
					break;
				}
				try
				{
					Log.d(CommonUtilities.TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				}
				catch (InterruptedException e1)
				{
					// Activity finished before we complete - exit.
					Log.d(CommonUtilities.TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return;
				}
				// increase backoff exponentially
				backoff *= 2;
			}
		}
		String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);
		CommonUtilities.displayMessage(context, message);
	}

	/**
	 * Unregister this account/device pair within the server.
	 */
	static void unregister(final Context context, final String regId)
	{
		Log.i(CommonUtilities.TAG, "unregistering device (regId = " + regId + ")");
		String serverUrl = CommonUtilities.SERVER_URL + "/unregister";

		try
		{
			post(context, serverUrl, regId);
//			GCMRegistrar.setRegisteredOnServer(context, false);
			String message = context.getString(R.string.server_unregistered);
			CommonUtilities.displayMessage(context, message);
		}
		catch (Exception e)
		{
			// At this point the device is unregistered from GCM, but still
			// registered in the server.
			// We could try to unregister again, but it is not necessary:
			// if the server tries to send a message to the device, it will get
			// a "NotRegistered" error message and should unregister the device.
			String message = context.getString(R.string.server_unregister_error, e.getMessage());
			CommonUtilities.displayMessage(context, message);
		}
	}

	/**
	 * Issue a POST request to the server.
	 * 
	 * @param endpoint
	 *            POST address.
	 * @param params
	 *            request parameters.
	 * 
	 * @throws IOException
	 *             propagated from POST.
	 */
	private static void post3(final Context context, String endpoint, String params)
	{

		String url = CommonUtilities.SERVER_URL + params + CommonUtilities.SERVER_SECURITY;
		System.out.println("****url****" + url);

		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(30000);
		client.get(url, new AsyncHttpResponseHandler()
		{

			@Override
			public void onSuccess(String arg0)
			{
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				System.out.println("****arg0****" + arg0);
				JSONObject oJsonObject;
				try
				{
					oJsonObject = new JSONObject(arg0);

					CommonUtilities.displayMessage(context, oJsonObject.getString("status"));
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(Throwable arg0)
			{
				// TODO Auto-generated method stub
				System.out.println("****234134543****" + arg0);
				super.onFailure(arg0);

			}

		});

	}

	private static void post(final Context context, String endpoint, String params)
	{
		String url = CommonUtilities.SERVER_URL + params + CommonUtilities.SERVER_SECURITY;
		
		AsyncTask<String, String, String> task = new AsyncTask<String, String, String>()
		{

			@Override
			protected String doInBackground(String... params)
			{
				// TODO Auto-generated method stub
				String result = JSONClass.getJSONfromURL(params[0]);
				
				return result;
			}

			@Override
			protected void onPostExecute(String result)
			{
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				
				JSONObject oJsonObject;
				try
				{
					oJsonObject = new JSONObject(result);

					CommonUtilities.displayMessage(context, oJsonObject.getString("status"));
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			

		};
		task.execute(url);
	}

}
