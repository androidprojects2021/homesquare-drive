package com.homesquad.retrofit;


import com.homesquad.model.NotificationsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Leo elstin on 9/4/18.
 */
public interface ApiInterface {

    @GET("get_driver_notifications")
    Call<NotificationsModel> getNotifications(@Query("imei") String imei);
}
