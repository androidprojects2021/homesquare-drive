package com.homesquad.retrofit;


import com.homesquad.utils.URLUtil;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Leo elstin on 2/26/18.
 */

public class ApiClient {

    public static Retrofit getClient() {

        Retrofit retrofit = new Retrofit.Builder()

                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URLUtil.BASEURL)
//                .baseUrl("http://dailyhelp.dyndns.org:8090/dailyhelp-demo/driverappapi/")

                .build();

        return retrofit;
    }


}
