package com.homesquad.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import com.homesquad.driver.R;


public class ButtonCustomFont extends Button
{
	private static final String TAG = "TextView";

	public ButtonCustomFont(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
	}

	public ButtonCustomFont(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setCustomFont(context, attrs);
	}

	public ButtonCustomFont(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setCustomFont(context, attrs);

	}

	private void setCustomFont(Context ctx, AttributeSet attrs)
	{
		TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
		String customFont = a.getString(R.styleable.TextViewPlus_customFont);
		Boolean bold = a.getBoolean(R.styleable.TextViewPlus_bold, false);
		setCustomFont(ctx, customFont, bold);
		a.recycle();
	}

	public boolean setCustomFont(Context ctx, String asset, Boolean b)
	{
		
		Typeface tf = null;
		try
		{
			tf = Typeface.createFromAsset(ctx.getAssets(), asset);

		}
		catch (Exception e)
		{
			Log.e(TAG, "Could not get typeface: " + e.getMessage());
			return false;
		}
		if (b)

		{
			setTypeface(tf, Typeface.BOLD);
		}
		else
		{
			setTypeface(tf);
		}

		return true;
	}
}
