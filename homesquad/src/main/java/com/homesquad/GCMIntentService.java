//package com.emaid;
//
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.support.v4.app.NotificationCompat;
//import android.support.v4.app.TaskStackBuilder;
//import android.util.Log;
//
//import com.dailyhelp.driver.R;
//import FlieNotification;
//import Myservices;
//import com.google.android.gcm.GCMBaseIntentService;
//
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//
//public class GCMIntentService extends GCMBaseIntentService {
//
//	private static final String TAG = "GCMIntentService";
//	static int notification_id = 0;
//
//	public GCMIntentService()
//	{
//		super(CommonUtilities.SENDER_ID);
//	}
//
//	/**
//	 * Method called on device registered
//	 **/
//	@Override
//	protected void onRegistered(Context context, String registrationId)
//	{
//		Log.e(TAG, "Device registered: regId = " + registrationId);
//
//		CommonUtilities.displayMessage(context, "Your device registred with GCM");
//		ServerUtilities.register(context, registrationId);
//	}
//
//	/**
//	 * Method called on device un registred
//	 * */
//	@Override
//	protected void onUnregistered(Context context, String registrationId)
//	{
//		Log.i(TAG, "Device unregistered");
//		CommonUtilities.displayMessage(context, getString(R.string.gcm_unregistered));
//		ServerUtilities.unregister(context, registrationId);
//	}
//
//	/**
//	 * Method called on Receiving a new message
//	 * */
//	@Override
//	protected void onMessage(Context context, Intent intent)
//	{
//		System.out.println("****435455555555555***");
//		Log.i(TAG, "Received message");
//		String message = intent.getExtras().getString("price");
//		int countw = 0;
//		String count1 = FlieNotification.readFromFile(getBaseContext());
//		if (count1.equals(""))
//		{
//			count1 = "0";
//		}
//		try
//		{
//			countw = Integer.parseInt(count1);
//		}
//		catch (Exception e)
//		{
//			countw = 0;
//		}
//
//		countw = countw + 1;
//		count1 = String.valueOf(countw);
//		FlieNotification.writeToFile(getApplicationContext(), count1);
//
//		// Intent myIntent = new Intent(getBaseContext(), Myservices.class);
//		// startService(myIntent);
//
//		// CommonUtilities.displayMessage(context, message);
//
//		generateNotification(context, message);
//	}
//
//	/**
//	 * Method called on receiving a deleted message
//	 * */
//	@Override
//	protected void onDeletedMessages(Context context, int total)
//	{
//		Log.i(TAG, "Received deleted messages notification");
//		String message = getString(R.string.gcm_deleted, total);
//		CommonUtilities.displayMessage(context, message);
//		// notifies user
//		generateNotification(context, message);
//	}
//
//	/**
//	 * Method called on Error
//	 * */
//	@Override
//	public void onError(Context context, String errorId)
//	{
//		Log.i(TAG, "Received error: " + errorId);
//		CommonUtilities.displayMessage(context, getString(R.string.gcm_error, errorId));
//	}
//
//	@Override
//	protected boolean onRecoverableError(Context context, String errorId)
//	{
//		// log message
//		Log.i(TAG, "Received recoverable error: " + errorId);
//		CommonUtilities.displayMessage(context, getString(R.string.gcm_recoverable_error, errorId));
//		return super.onRecoverableError(context, errorId);
//	}
//
//	/**
//	 * Issues a notification to inform the user that server has sent a message.
//	 */
//	private static void generateNotification(final Context context, String message)
//	{
//
//		try
//		{
//
//			if (BaseActivity.appInFront)
//			{
//				System.out.println("*****cccc**");
//
//				try
//				{
//					playsound(context);
//				}
//				catch (IllegalArgumentException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				Intent myIntent = new Intent(context, Myservices.class);
//				myIntent.putExtra("message", message);
//				myIntent.putExtra("heading", "Notification");
//				context.startService(myIntent);
//
//			}
//			else
//			{
//				int notification_id = 0;
//				Calendar c = Calendar.getInstance();
//
//				SimpleDateFormat df = new SimpleDateFormat("kkmmss");
//				String strTodayDate = df.format(c.getTime());
//
//				try
//				{
//					notification_id = Integer.parseInt(strTodayDate);
//				}
//				catch (Exception e)
//				{
//					notification_id = 0;
//				}
//
//				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher).setContentTitle(message).setContentText("");
//				mBuilder.setAutoCancel(true);
//				Intent resultIntent = new Intent(context, HomepageActivity.class);
//
//				resultIntent.putExtra("page", "notification");
//
//				// Add the bundle to the intent
//
//				TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//
//				stackBuilder.addParentStack(Synchronize.class);
//
//				stackBuilder.addNextIntent(resultIntent);
//				PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//				mBuilder.setContentIntent(resultPendingIntent);
//				Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//				mBuilder.setSound(alarmSound);
//				NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//				mNotificationManager.notify(notification_id, mBuilder.build());
//			}
//
//		}
//		catch (Exception e)
//		{
//			int notification_id = 0;
//			Calendar c = Calendar.getInstance();
//
//			SimpleDateFormat df = new SimpleDateFormat("kkmmss");
//			String strTodayDate = df.format(c.getTime());
//
//			try
//			{
//				notification_id = Integer.parseInt(strTodayDate);
//			}
//			catch (Exception es)
//			{
//				notification_id = 0;
//			}
//
//			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher).setContentTitle(message).setContentText("");
//			mBuilder.setAutoCancel(true);
//			Intent resultIntent = new Intent(context, HomepageActivity.class);
//
//			resultIntent.putExtra("page", "notification");
//
//			// Add the bundle to the intent
//
//			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//
//			stackBuilder.addParentStack(Synchronize.class);
//
//			stackBuilder.addNextIntent(resultIntent);
//			PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//			mBuilder.setContentIntent(resultPendingIntent);
//			Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//			mBuilder.setSound(alarmSound);
//			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//			mNotificationManager.notify(notification_id, mBuilder.build());
//		}
//
//	}
//
//	public static void playsound(Context context)
//	{
//		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//		RingtoneManager.getRingtone(context, alarmSound).play();
//
//	}
//
//}
