package com.homesquad.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.driver.R;

public class FragmentDateSelection extends Fragment implements BaseInterface
{
	View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_dataselectionchild, null);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initUI();
		clickFunction();

	}

	@Override
	public void initUI()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void clickFunction()
	{
		// TODO Auto-generated method stub

	}

}
