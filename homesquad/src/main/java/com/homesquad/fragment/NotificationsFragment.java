package com.homesquad.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.homesquad.adapters.NotificationAdapter;
import com.homesquad.driver.R;
import com.homesquad.model.Notification;
import com.homesquad.model.NotificationsModel;
import com.homesquad.retrofit.ApiClient;
import com.homesquad.retrofit.ApiInterface;
import com.homesquad.utils.URLUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsFragment extends Fragment {

    List<Notification> notificationList = new ArrayList<>();

    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ProgressBar progressBar = view.findViewById(R.id.progressBar);
        final TextView erroText = view.findViewById(R.id.errorText);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        final NotificationAdapter adapter = new NotificationAdapter(notificationList,getActivity());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);
        erroText.setVisibility(View.GONE);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getNotifications(URLUtil.strPhoneIMEI).enqueue(new Callback<NotificationsModel>() {
            @Override
            public void onResponse(Call<NotificationsModel> call, Response<NotificationsModel> response) {

                progressBar.setVisibility(View.GONE);
                erroText.setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        if (response.body().status.equalsIgnoreCase("success")) {

                            notificationList.addAll(response.body().notifications);
                            adapter.notifyDataSetChanged();
                        }
                    }

                }

                Log.e("Notification res", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<NotificationsModel> call, Throwable t) {

//                Log.e("Notification Error", t.getLocalizedMessage());
                erroText.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}