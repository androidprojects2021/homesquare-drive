package com.homesquad.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.driver.R;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

public class AttendanceReportFragment extends Fragment implements BaseInterface {
    View view;
    GridView gridView;

    public static AttendanceReportFragment instance() {
        AttendanceReportFragment fragmentAttendancereport = new AttendanceReportFragment();

        return fragmentAttendancereport;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        view = inflater.inflate(R.layout.fragment_attendancereport, null);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        GlobalManager.getdata().setCurrentActivity("AttendanceReportFragment");
        initUI();
        clickFunction();

    }

    @Override
    public void initUI() {
        // TODO Auto-generated method stub
        gridView = (GridView) view.findViewById(R.id.paymenthistoryfragment_gridview);

    }

    @Override
    public void clickFunction() {
        // TODO Auto-generated method stub
        // getData(URLUtil.getPaymentHistory());
        getData(URLUtil.getAttendanceReport());

    }

    public void getData(String URL) {
        CommonUtil.progressDialogue(getActivity());
        CommonUtil.dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.get(URL, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onFailure(arg0, arg1);
                CommonUtil.dialog.dismiss();
                CommonUtil.CustomeDialogue(getActivity(), "Error", "Network Error");
            }

            @Override
            public void onSuccess(String arg0) {
                // TODO Auto-generated method stub
                super.onSuccess(arg0);
                CommonUtil.dialog.dismiss();
                Gson gson = new Gson();
                try {
                    EmaidCustomerAttendanceReportDetailsURL oEmaidCustomerAttendanceReportDetailsURL = gson.fromJson(arg0, EmaidCustomerAttendanceReportDetailsURL.class);
                    if (oEmaidCustomerAttendanceReportDetailsURL.getStatus().equals("success")) {
                        GridviewAdapter adapter = new GridviewAdapter(oEmaidCustomerAttendanceReportDetailsURL.getAttandencemaid());
                        gridView.setAdapter(adapter);
                        double amount = 0;
                        // for (int i = 0; i <
                        // oEmaidCustomerAttendanceReportDetailsURL.getPayments().size();
                        // i++)
                        // {
                        // if
                        // (oEmaidCustomerAttendanceReportDetailsURL.getPayments().get(i).getPayment_status().equals("1"))
                        // {
                        // amount = amount +
                        // Double.parseDouble(oEmaidCustomerAttendanceReportDetailsURL.getPayments().get(i).getPaid_amount());
                        // }
                        //
                        // }
                        // textViewTotal.setText("Total : " +
                        // String.valueOf(amount));
                    } else {

                        CommonUtil.CustomeDialogue(getActivity(), "Error", oEmaidCustomerAttendanceReportDetailsURL.getStatus());
                    }
                } catch (Exception e) {
                    CommonUtil.CustomeDialogue(getActivity(), "Error", "Network Error");
                }

            }

        });
    }

    public class GridviewAdapter extends BaseAdapter {
        LayoutInflater inflater;
        List<EmaidCustomerAttendanceReportDetailsURLContainer> list = new ArrayList<EmaidCustomerAttendanceReportDetailsURLContainer>();

        public GridviewAdapter(List<EmaidCustomerAttendanceReportDetailsURLContainer> list1) {
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // if (list1.get(i).getPayment_status().equals("1"))
            // {
            // }
            list.addAll(list1);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            View view = inflater.inflate(R.layout.adapter_grid_attendancereport, null);
            LinearLayout layout = (LinearLayout) view.findViewById(R.id.adapterpayment_layout_customername);
            Button buttonCustomername = (Button) view.findViewById(R.id.adapterpayment_btn_customername);
            // Button buttonCustomerAddress = (Button)
            // view.findViewById(R.id.adapterpayment_btn_customeraddress);
            Button buttonCustomerPhoneNo = (Button) view.findViewById(R.id.adapterpayment_btn_customerphoneno);
            // Button buttonCustomernameAmount = (Button)
            // view.findViewById(R.id.adapterpayment_btn_customeramount);

            // if (list.get(arg0).getPayment_status().equals("1"))
            // {
            layout.setBackgroundColor(getResources().getColor(R.color.blue));

            buttonCustomername.setText(list.get(arg0).getMaid_name());
            // buttonCustomerAddress.setText(list.get(arg0).getCustomer_address());
            buttonCustomerPhoneNo.setText(list.get(arg0).getAttandence_status());
            // buttonCustomernameAmount.setText(list.get(arg0).getPaid_amount());
            // }

            return view;
        }

    }

}
