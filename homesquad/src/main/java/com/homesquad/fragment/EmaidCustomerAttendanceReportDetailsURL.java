package com.homesquad.fragment;

import java.util.List;

public class EmaidCustomerAttendanceReportDetailsURL {

	private String status = "";

	private List<EmaidCustomerAttendanceReportDetailsURLContainer> attandence;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public List<EmaidCustomerAttendanceReportDetailsURLContainer> getAttandencemaid()
	{
		return attandence;
	}

	public void setAttandencemaid(List<EmaidCustomerAttendanceReportDetailsURLContainer> attandence)
	{
		this.attandence = attandence;
	}

}
