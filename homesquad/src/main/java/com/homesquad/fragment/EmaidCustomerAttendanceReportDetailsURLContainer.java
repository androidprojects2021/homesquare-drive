package com.homesquad.fragment;

public class EmaidCustomerAttendanceReportDetailsURLContainer {

	private String maid_name = "";
	private String attandence_status = "";
	private String maid_id  = "";
	private String tablet_id = "";
	private String zone_name = "";
	private String scheduled_zone = "";
	
	

	public String getMaid_name()
	{
		return maid_name;
	}

	public void setMaid_name(String maid_name)
	{
		this.maid_name = maid_name;
	}

	public String getAttandence_status()
	{
		return attandence_status;
	}

	public void setAttandence_status(String attandence_status)
	{
		this.attandence_status = attandence_status;
	}
	
	public String getMaid_id()
	{
		return maid_id;
	}

	public void setMaid_id(String maid_id)
	{
		this.maid_id = maid_id;
	}
	
	public String getTablet_id()
	{
		return tablet_id;
	}

	public void setTablet_id(String tablet_id)
	{
		this.tablet_id = tablet_id;
	}
	
	public String getZone_name()
	{
		return zone_name;
	}

	public void setZone_name(String zone_name)
	{
		this.zone_name = zone_name;
	}
	
	public String getScheduled_zone()
	{
		return scheduled_zone;
	}

	public void setScheduled_zone(String scheduled_zone)
	{
		this.scheduled_zone = scheduled_zone;
	}
}
