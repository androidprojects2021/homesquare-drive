package com.homesquad.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.homesquad.baseinterface.BaseInterface;
import com.homesquad.baseinterface.GlobalManager;
import com.homesquad.driver.R;
import com.homesquad.utils.CommonUtil;
import com.homesquad.utils.URLUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import model.PaymentHistory;
import model.PaymentHistoryModel;

public class PaymentHistoryFragment extends Fragment implements BaseInterface {

	View view;
	GridView gridView;
	TextView textViewTotal;
	LinearLayout noresultLay;

	public static PaymentHistoryFragment instance() {
		PaymentHistoryFragment fragmentTodayschedule = new PaymentHistoryFragment();

		return fragmentTodayschedule;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_paymenthistory, null);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		GlobalManager.getdata().setCurrentActivity("PaymentHistoryFragment");
		initUI();
		clickFunction();

	}

	@Override
	public void initUI() {
		gridView = (GridView) view
				.findViewById(R.id.paymenthistoryfragment_gridview);
		textViewTotal = (TextView) view
				.findViewById(R.id.paymenthistoryfragment_total);
		noresultLay = (LinearLayout) view.findViewById(R.id.noresultlay);
		noresultLay.setVisibility(View.GONE);

	}

	@Override
	public void clickFunction() {
		getData(URLUtil.getPaymentHistory());

	}

	public void getData(String URL) {
		CommonUtil.progressDialogue(getActivity());
		CommonUtil.dialog.show();
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(30000);
		client.get(URL, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);
				CommonUtil.dialog.dismiss();
				CommonUtil.CustomeDialogue(getActivity(), "Error",
						"Failed to connect server.");
			}

			@Override
			public void onSuccess(String arg0) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				CommonUtil.dialog.dismiss();
				Gson gson = new Gson();
				try {
					PaymentHistoryModel oEmaidCustomerPaymentDetailsURL = gson
							.fromJson(arg0,
									PaymentHistoryModel.class);
					if (oEmaidCustomerPaymentDetailsURL.getStatus().equals(
							"success")) {

						if (oEmaidCustomerPaymentDetailsURL.getPayments() //abhi added
								.size() == 0) {
							noresultLay.setVisibility(View.VISIBLE);
						} else {
							noresultLay.setVisibility(View.GONE); //gone here
						}

						GridviewAdapter adapter = new GridviewAdapter(
								oEmaidCustomerPaymentDetailsURL.getPayments());
						gridView.setAdapter(adapter);
						Double amount = 0.0;
						for (int i = 0; i < oEmaidCustomerPaymentDetailsURL
								.getPayments().size(); i++) {
							if (oEmaidCustomerPaymentDetailsURL.getPayments()
									.get(i).getPayment_status().equals("1")) {
								amount = amount + Double.parseDouble(oEmaidCustomerPaymentDetailsURL
										.getPayments().get(i)
										.getPaid_amount());
							}

						}
						textViewTotal.setText("Total : "
								+ String.valueOf(amount.toString()));
					} else {

						CommonUtil.CustomeDialogue(getActivity(), "Error",
								oEmaidCustomerPaymentDetailsURL.getStatus());
					}
				} catch (Exception e) {
					CommonUtil.CustomeDialogue(getActivity(), "Error",
							e.getMessage());
				}

			}

		});
	}

	public class GridviewAdapter extends BaseAdapter {
		LayoutInflater inflater;
		List<PaymentHistory> list = new ArrayList<PaymentHistory>();

		public GridviewAdapter(
				List<PaymentHistory> list1) {
			inflater = (LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);

			for (int i = 0; i < list1.size(); i++) {
				if (list1.get(i).getPayment_status().equals("1")) {
					list.add(list1.get(i));
				}
			}

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			View view = inflater.inflate(R.layout.adapter_grid_payment, null);
			LinearLayout layout = (LinearLayout) view
					.findViewById(R.id.adapterpayment_layout_customername);
			Button buttonCustomername = (Button) view
					.findViewById(R.id.adapterpayment_btn_customername);
			Button buttonCustomerAddress = (Button) view
					.findViewById(R.id.adapterpayment_btn_customeraddress);
			Button buttonCustomerPhoneNo = (Button) view
					.findViewById(R.id.adapterpayment_btn_customerphoneno);
			Button buttonCustomernameAmount = (Button) view
					.findViewById(R.id.adapterpayment_btn_customeramount);

			Button adapterpayment_btn_customeramount_collected = (Button) view
					.findViewById(R.id.adapterpayment_btn_customeramount_collected);

			Button adapterpayment_btn_maid = view.findViewById(R.id.adapterpayment_btn_maid);


			if (list.get(arg0).getPayment_status().equals("1")) {
				layout.setBackgroundColor(getResources().getColor(
						R.color.liteaccent));

				buttonCustomername.setText(list.get(arg0).getCustomer_name());
				buttonCustomerAddress.setText(list.get(arg0)
						.getCustomer_address());
				buttonCustomerPhoneNo.setText(list.get(arg0)
						.getCustomer_mobile());
				buttonCustomernameAmount.setText(String.format("Collected Amount : AED %s", list.get(arg0)
						.getPaid_amount()));

				adapterpayment_btn_customeramount_collected.setText(String.format("Billed Amount : AED %s", list.get(arg0)
						.getBooking_amount()));

				adapterpayment_btn_maid.setText(list.get(arg0).getMaid_name());
			}

			return view;
		}

	}
}
