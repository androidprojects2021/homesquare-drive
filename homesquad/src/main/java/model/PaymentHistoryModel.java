package model;

import java.util.List;

public class PaymentHistoryModel {
    private String status = "";
    private List<PaymentHistory> payments;

    public PaymentHistoryModel() {
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PaymentHistory> getPayments() {
        return this.payments;
    }

    public void setPayments(List<PaymentHistory> payments) {
        this.payments = payments;
    }
}
