package model;

public class EmaidCustomerDetailsURLContainerNew {
    private String service_status = "";
    private String booking_id = "";
    private String customer_id = "";
    private String customer_code = "";
    private String customer_name = "";
    private String customer_address = "";
    private String customer_mobile = "";
    private String customer_type = "";
    private String maid_id = "";
    private String maid_name = "";
    private String maid_country = "";
    private String shift_start = "";
    private String shift_end = "";
    private String key_status = "";
    private String area = "";
    private String booking_note = "";
    private String service_fee = "";
    private String cleaning_material = "";
    private String apartment_number = "";

    private String maid_attandence = "";
    private String customer_status = "";

    private String customer_longitude = "";
    private String customer_latitude = "";

    public String getApartment_number() {
        return apartment_number;
    }

    public void setApartment_number(String apartment_number) {
        this.apartment_number = apartment_number;
    }

    public String getMaid_attandence() {
        return maid_attandence;
    }

    public void setMaid_attandence(String maid_attandence) {
        this.maid_attandence = maid_attandence;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }

    public String getCustomer_status() {
        return customer_status;
    }

    public void setCustomer_status(String customer_status) {
        this.customer_status = customer_status;
    }

    public String getMaid_id() {
        return maid_id;
    }

    public void setMaid_id(String maid_id) {
        this.maid_id = maid_id;
    }

    public String getMaid_name() {
        return maid_name;
    }

    public void setMaid_name(String maid_name) {
        this.maid_name = maid_name;
    }

    public String getMaid_country() {
        return maid_country;
    }

    public void setMaid_country(String maid_country) {
        this.maid_country = maid_country;
    }

    public String getMaid_status() {
        return maid_attandence;
    }

    public void setMaid_status(String maid_status) {
        this.maid_attandence = maid_status;
    }

    public String getShift_start() {
        return shift_start;
    }

    public void setShift_start(String shift_start) {
        this.shift_start = shift_start;
    }

    public String getShift_end() {
        return shift_end;
    }

    public void setShift_end(String shift_end) {
        this.shift_end = shift_end;
    }

    public String getKey_status() {
        return key_status;
    }

    public void setKey_status(String key_status) {
        this.key_status = key_status;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBooking_note() {
        return booking_note;
    }

    public void setBooking_note(String booking_note) {
        this.booking_note = booking_note;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getCustomer_longitude() {
        return customer_longitude;
    }

    public void setCustomer_longitude(String customer_longitude) {
        this.customer_longitude = customer_longitude;
    }

    public String getCustomer_latitude() {
        return customer_latitude;
    }

    public void setCustomer_latitude(String customer_latitude) {
        this.customer_latitude = customer_latitude;
    }

    public String getService_fee() {
        return service_fee;
    }

    public void setService_fee(String service_fee) {
        this.service_fee = service_fee;
    }

    public String getCleaning_material() {
        return cleaning_material;
    }

    public void setCleaning_material(String cleaning_material) {
        this.cleaning_material = cleaning_material;
    }


}
