package model;

public class PaymentHistory {
    private String booking_id = "";
    private String service_status = "";
    private String customer_id = "";
    private String customer_code = "";
    private String customer_name = "";
    private String customer_address = "";
    private String customer_mobile = "";
    private String customer_type = "";
    private String paid_amount = "";
    private Double booking_amount = 0.0;
    private String payment_status = "";
    private String maid_id = "";
    private String maid_name = "";
    private String maid_country = "";
    private String shift_start = "";
    private String shift_end = "";

    public PaymentHistory() {
    }

    public String getBooking_id() {
        return this.booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getService_status() {
        return this.service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getCustomer_id() {
        return this.customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_code() {
        return this.customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getCustomer_name() {
        return this.customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_address() {
        return this.customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_mobile() {
        return this.customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getCustomer_type() {
        return this.customer_type;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }

    public String getPaid_amount() {
        return this.paid_amount;
    }

    public void setPaid_amount(String paid_amount) {
        this.paid_amount = paid_amount;
    }

    public String getPayment_status() {
        return this.payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getMaid_id() {
        return this.maid_id;
    }

    public void setMaid_id(String maid_id) {
        this.maid_id = maid_id;
    }

    public String getMaid_name() {
        return this.maid_name;
    }

    public void setMaid_name(String maid_name) {
        this.maid_name = maid_name;
    }

    public String getMaid_country() {
        return this.maid_country;
    }

    public void setMaid_country(String maid_country) {
        this.maid_country = maid_country;
    }

    public String getShift_start() {
        return this.shift_start;
    }

    public void setShift_start(String shift_start) {
        this.shift_start = shift_start;
    }

    public String getShift_end() {
        return this.shift_end;
    }

    public void setShift_end(String shift_end) {
        this.shift_end = shift_end;
    }

    public Double getBooking_amount() {
        return booking_amount;
    }

    public void setBooking_amount(Double booking_amount) {
        this.booking_amount = booking_amount;
    }
}
