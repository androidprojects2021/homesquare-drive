package model;

import java.util.List;


public class EmaidCustomerDetailsURLNew {
	
	private String status = "";
	
	List<EmaidCustomerDetailsURLContainerNew> schedule ;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public List<EmaidCustomerDetailsURLContainerNew> getSchedule()
	{
		return schedule;
	}

	public void setSchedule(List<EmaidCustomerDetailsURLContainerNew> schedule)
	{
		this.schedule = schedule;
	}
	
}
